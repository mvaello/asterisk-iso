#!/bin/sh

#apt-get install -y linux-headers-$(uname -r) libnewt-dev libspandsp-dev unixodbc-dev

#
# Instala dependencias que no se dejaban instalar por por fichero packages
#
apt-get install -y libnewt-dev libspandsp-dev unixodbc-dev console-data console-setup console-locales keyboard-configuration

## Configura la dispisición del teclado, la zona horaria y las locales
dpkg-reconfigure console-data
dpkg-reconfigure tzdata
dpkg-reconfigure locales

whiptail --title "Paquetes extra" --msgbox "Se van a instalar los siguientes paquetes:'\n\n DAHDI, LIBPRI, FREEPBX e Asterisk.\n" 8 78 0 

##
# Instala DAHDI desde el comprimido predescargado
#
cd /usr/src

# Descopmprime
tar zxvf dahdi-linux-complete*
cd /usr/src/dahdi-linux-complete*
## Compila con 2 jobs (-j2) ya que al menos dos cores tendrá, install y configura 
make -j2 && make install && make config
 
#####
# Instala LIBPRI desde el comprimido predescargado y se compila
#
######
cd /usr/src
tar xvfz libpri-1.4-current.tar.gz
cd libpri-1.4*
make -j2 && make install

#####
# Descomprime FREEPBX desde el comprimido predescargado, luego se instalará.
#
######
cd /usr/src
tar zxvf freepbx-2.11*

#####
# Descomprime ASTERISK desde el comprimido predescargado, luego se instalará.
#
######
cd /usr/src
tar zxvf asterisk-11-current.tar.gz
cd /usr/src/asterisk-11*
make clean && make distclean

# Ejecuta los scrpt y te sacará luego el menuselect 
./contrib/scripts/get_mp3_source.sh
./configure CFLAGS=-mtune=native && make menuselect

cd /usr/src/asterisk-11*
make -j2 && make install && make config

####### Instala MOH
ln -s /var/lib/asterisk/moh /var/lib/asterisk/mohmp3


######## Configura el usuario como asterisk no sé si es exactamente el asteriskuser o no.
adduser asterisk --disabled-password --no-create-home --home /var/lib/asterisk --shell \
/sbin/nologin --gecos "Asterisk User"

####### Cambia algunas cmieda en los ficheros de entorno de apache
sed -i 's/\(APACHE_RUN_USER=\)\(.*\)/\1asterisk/g' /etc/apache2/envvars
sed -i 's/\(APACHE_RUN_GROUP=\)\(.*\)/\1asterisk/g' /etc/apache2/envvars
# Pone como propietario del apache al usuarios asteisk antes creado y reinicial el servicio
chown asterisk. /var/lock/apache2
service apache2 restart

###### Instala MySQLL


# Esto permite que no te salte el cuadro de diálogo por defectod e MySQL que te
# pide la contraseña porque en ese instante e imposoble saber la cotraseña y
# por tanto con "debconf-set-selections" trampeas y le fuerzas a usar unas que
# luego le pasa como parémetro con la guarrería de esta fucnión. 
# Si te fijas pone root_password passwor $1, entiendo que le pases la
# constraseña que necesita pero no sé si es de root o qué.
preseed_mysql_server ()
{
  MYSQL_SERVER_PACKAGE="`apt-cache show mysql-server | grep ^Depends | awk '{print $2}' | head -1`"
  cat << EOF | debconf-set-selections
$MYSQL_SERVER_PACKAGE mysql-server/root_password password $1
$MYSQL_SERVER_PACKAGE mysql-server/root_password_again password $1
$MYSQL_SERVER_PACKAGE mysql-server/root_password seen true
$MYSQL_SERVER_PACKAGE mysql-server/root_password_again seen true
$MYSQL_SERVER_PACKAGE mysql-server/start_on_boot boolean true
EOF

}
 
####
# Aquí escribes la password se MySQL
###
MYSQL_ROOT_PW=$(whiptail --nocancel --passwordbox "Debe intrducir una clave para MySQL" 8 78 --title "Configurando MySQL" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    while [ -z $MYSQL_ROOT_PW ];
    do
        MYSQL_ROOT_PW=$(whiptail --passwordbox "Por favor intrduzca una clave para MySQL" 8 78 --title "Configurando MySQL" 3>&1 1>&2 2>&3)
    done
    # Aquí es donde le va a pasar la clave a la función guarra de arriba que pilla estos parámetros. Lo que hecho igual que los de VozBox 
    preseed_mysql_server ${MYSQL_ROOT_PW} MYSQL_SERVER_PACKAGE
fi
##########################

# Ahora instala MySql y todas las dependencias pero con la clave pre-puesta.
# Si no hacemos lo de antes, después al hacer los INSERTS no sabríamos la clave.
#
aptitude install -y -q apache2-utils php5-mysql mysql-server mysql-common unixodbc unixodbc-dev libmyodbc mysql-client

# Reinicia MySQL
service mysql restart

cd /usr/src/freepbx*
#mysqladmin -u root -p${MYSQL_ROOT_PW} create asterisk
#mysqladmin -u root -p${MYSQL_ROOT_PW} create asteriskcdrdb



ASTERISK_DB_PW="amp109" # Usuario de apmortal a capón  
ASTERISK_DB_USER="asteriskuser" # No lo tengo claro
MYSQL_ROOT_PW="obelix" # En caso de que no quieras que lo pida.

##
# Crea las tablas y mete más cosas. Pero usa las claves y los usuarios que
# están en las variables que se indican con el ${-----}. Aparentemente puede
# que no sean esas porque aquí es donde cascaba.
##
echo "CREATE DATABASE IF NOT EXISTS asteriskcdrdb;" | mysql -u root -p${MYSQL_ROOT_PW}
echo "CREATE DATABASE IF NOT EXISTS asterisk;" | mysql -u root -p{$MYSQL_ROOT_PW}
mysql -u${ASTERISK_DB_USER} -p asterisk < SQL/newinstall.sql
mysql -u${ASTERISK_DB_USER} -p asteriskcdrdb < SQL/cdr_mysql_table.sql

echo "GRANT ALL PRIVILEGES ON asterisk.* TO ${ASTERISK_DB_USER}@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO ${ASTERISK_DB_USER}@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "flush privileges;" | mysql -u root -p${MYSQL_ROOT_PW}

####### Continua instalción Freepbx
cd /usr/src/freepbx*

# Crea uficherlo log en /root
touch /root/amp.log
chmod 777 /root/amp.log
chmod 755 install_amp
# supuestamente se le pasan esos usaurios y pista pero no tiene pinta.
./install_amp --username=${ASTERISK_DB_USER} --password=${ASTERISK_DB_PW} >> amp.log
#./install_amp

# Modifica el fichero directamente
cat > /etc/rc.local << EOF
#!/bin/sh -e
/usr/local/sbin/amportal start
exit 0
EOF

# Pone la raíz por defecto en apache en /var/www/html
sed -i "s_/var/www_/var/www/html_" /etc/apache2/sites-available/default

#sed -i -e "s/\/var/www/\/var/www/html/g" /etc/apache2/sites-avaliable/default

touch /etc/asterisk/manager_addional.conf
touch /etc/asterisk/manager_custom.conf

#
#sleep 0.5
#apt-get clean &>/dev/null 2>&1
#apt-get autoclean &>/dev/null 2>&1
#apt-get clean &>/dev/null 2>&1
#mv /usr/local/sbin/postinst-pbx.sh /usr/local/sbin/postinstd.sh

#whiptail  --msgbox "Instalacion completada, ahora se reinciciara la maquina" 8 78 0 
#shutdown -r now;
#exit 0
