#!/bin/bash
# vozbox_clean.sh
#
# Este script es software libre. Puede redistribuirlo y/o
# modificarlo bajo los términos de la Licencia Pública General
# de GNU según es publicada por la Free Software Foundation,
# bien de la versión 3 de dicha Licencia o bien (según su
# elección) de cualquier versión posterior.
#
# This project is free software. You can redistribute it and/or
# modify it under the terms of the General Public License
# License as published by the Free Software Foundation;
# Either version 3 of the License, or (at your Option) any later version.
#
# This project is distributed in the hope that it
# useful, but WITHOUT ANY WARRANTY, without even the guarantee
# MERCHANTABILITY no guarantee implied or FITNESS FOR A
# PARTICULAR PURPOSE. For details, see License
# GNU General Public.
#
# Author: Daver Jorge <davervozbox@gmail.com>
# With the collaboration of Carlos Perez <carlos.perez@uv.es>
# Copyright 2012 Daver Jorge <davervozbox@gmail.com>
# This script is licensed under GNU GPL version 3.0
#
# Este script ha utilizado y modificado código GNU GPL version 2.0
# originalmente desarrollado para el proyecto DebPBX,
# creado por Federico Pereira <fpereira@opentecnologic.com>
########################################################################
# -------------lunes 17 de marzo de 2014--------12:00
########################################################################
#
# Preparación para internacionalización
. gettext.sh
export TEXTDOMAIN="vozbox"
#export TEXTDOMAINDIR="./locale/i18n"
# Fin de la i18n
# Carlos Pérez, 15 de mayo de 2013

#cat > /root/.bash_login <<ENDLINE
##!/bin/bash
#clear
#echo "VozBox"
#echo "Author: Daver Jorge <davervozbox@gmail.com>"
#echo "Licensed under GNU GPL version 3.0"
#echo "Web Site: www.vozbox.es"
#echo ""
#echo "Comandos de Uso: vozbox-help"
#echo "Using Commands: vozbox-help"
#echo ""
#ENDLINE

########################  COMANDOS AYUDA  ##############################

echo "chown -R asterisk:asterisk /var/lib/asterisk/sounds/ /var/lib/asterisk/mohmp3/" > /usr/local/bin/permisonidos
echo "chmod -R 775 /var/lib/asterisk/sounds/ /var/lib/asterisk/mohmp3/" >> /usr/local/bin/permisonidos
chmod +x /usr/local/bin/permisonidos

echo "asterisk -rvvvvvvvvvvvvvvvvvvv" > /usr/local/bin/a
chmod +x /usr/local/bin/a

echo "cd /var/spool/hylafax/log; for i in \`ls -1 | grep c0 | tail -1\`; do tail -f /var/spool/hylafax/log/\$i; done" > /usr/local/bin/hylafaxlog
chmod +x /usr/local/bin/hylafaxlog

########################################################################
# DIRECTORIO PÚBLICO
DIRECTORIO="http://prdownloads.sourceforge.net/vozbox";

###################### REPOSITORIO DEB-MULTIMEDIA ######################
cp /etc/apt/sources.list /etc/apt/sources.list-orig

cat >> /etc/apt/sources.list << EOF
##MULTIMEDIA
deb http://www.deb-multimedia.org stable main non-free
EOF
############################ VERSIONES #################################

# freepbx
VER_VERSION="2.11";
VER_FREEPBX="2.11";

# dahdi-linux
VER_DAHDI_LINUX_COMPLETE="2.9.0.1";

# libpri
VER_LIBPRI="1.4.14";

# Asterisk-cert
VER_ASTERISK_CERT="certified-asterisk-11.6-current.tar.gz";
BRANCH_ASTERISK="11.6";

# fop2
VER_FOP2_ADMIN="1.2.15";
VER_FOP2="2.27";

# maualvozbox
VER_MANUAL="2.11.0";

# DEFINICION SANGOMA
SANGOMA_TARBALLS="ftp://ftp.sangoma.com/linux/current_wanpipe/wanpipe-current.tgz";
SANGOMA_BUILD_DEPENDS="flex bison";

# webmin
VER_WEBMIN="1.680";

# vozboxpbxskin
VER_SKIN="2.11.latest";

# spandsp
VER_SPAN="0.0.6";

# phpsysinfo
VER_PHPSYSINFO="3.1.8";

KERNEL_VERSION=`uname -r`
KERNEL_VERSION2=`uname -r`
KERNEL_VERSION=`echo ${KERNEL_VERSION} | sed -e "s/\-\(.*\)//"`

#dependencias
PAQUETES="build-essential linux-headers-`uname -r` openssh-server apache2 bison tftpd flex php5 php5-curl php5-ldap libtiff-tools \
php5-cli php-pear ntp nmap ntpdate php-db php5-gd php-gettext curl sox libncurses5-dev libssl-dev libmysqlclient-dev mpg123 libxml2-dev \
libnewt-dev sqlite3 libsqlite3-dev pkg-config automake libtool avahi-daemon avahi-discover libnss-mdns autoconf git subversion udev mc gawk fake-hwclock"

##################### DECLARACION DE VARIABLES #########################

MYNAME="FreePBX";
MYLOGFILE="/root/install-$MYNAME.log";
echo -n > "$MYLOGFILE"
STOP="0";
#TITLE="$(gettext "VozBox. Servicio Técnico VozBox -- http://www.vozbox.es")"
TITLE="$(gettext "")"
VALFILE=$(mktemp -t changeip.tmp.XXXXXXXXXXXXXXX)
UI_ALTO="14"
UI_ANCHO="70"
UI_MENU_ITEMS="5"
WGET_LOGFILE="wget.log";

# Usa arrays asociativos para anotar las descargas pendientes
declare -A DESCARGAS

# Asterisk pre-configuration
OPCIONES_MENUSELECT="--enable res_http_websocket --enable chan_sip --enable cdr_mysql --enable app_mysql --enable res_fax_spandsp --disable cel_pgsql --disable res_config_pgsql --disable cel_tds --disable cdr_radius --disable cel_radius"

#BASES DE DATOS
ASTERISK_DB="asterisk";

#USUARIOS BASES DE DATOS
ASTERISK_DB_USER="asterisk";

#MANAGER FREEPBX-VOZBOX
AMPMGR_USER="asterisk";

#AMPWEBROOT_RUTA FREEPBX-VOZBOX
AMP_WEB="/var/www/html";

#FOPWEBROOT_RUTA FOP
FOP_WEB="/var/www/fop2";

#PUERTO SSH
PUERTOSSH=$(cat /etc/ssh/sshd_config | grep '^Port' | grep -o '[0-9]*$');

#USUARIOS POR DEFECTO DE LAS GUI
ARI_ADMIN_USERNAME="admin";
FREEPBX_USER_GUI="admin";

#CPU_INFO
CPUINFO=`uname -m`

#PASSWORDS_SISTEMA
MYSQL_ROOT_PW=`head -c 200 /dev/urandom | tr -cd 'A-Za-z0-9' | head -c 20`
ASTERISK_DB_PW=`head -c 200 /dev/urandom | tr -cd 'A-Za-z0-9' | head -c 20`
ASTERISK_MGR_PW=`head -c 200 /dev/urandom | tr -cd 'A-Za-z0-9' | head -c 20`
ARI_PW=`head -c 200 /dev/urandom | tr -cd 'A-Za-z0-9' | head -c 20`
FOP2_PW=`head -c 200 /dev/urandom | tr -cd 'A-Za-z0-9' | head -c 20`
VTIGER_DB_PW=`head -c 200 /dev/urandom | tr -cd 'A-Za-z0-9' | head -c 20`

########################## FIN VARIABLES ###############################
########################################################################

#whiptail --title "$TITLE" --msgbox "'INSTALACIÓN DE SISTEMA DE COMUNICACIONES IP VOZBOX'\n\nA continuación se iniciará la instalación del sistema VozBox $VER_VERSION\n\nPARTE 1. INSTALACIÓN DE Dependencias.\nPARTE 2. INSTALACIÓN DE Asterisk.\nPARTE 3. INSTALACIÓN DE FreePBX-VozBox." 14 80 0

# VERSION
echo "${VER_VERSION}" > /etc/vozbox_version

############################ FUNCIONES #################################
# Definiciones de RED

function seleccionar_interfaz
{
        for x in $(ifconfig -a -s | egrep -v "^Iface|^lo" | awk '{print $1};')
        do
                # acumular las opciones en un array, pues --menu pide tag item por cada entrada
                opciones+=($x "")
        done
        whiptail  --backtitle "$TITLE" --menu "$(gettext "Elige la interfaz que pertenece a la red de Voz:")" $UI_ALTO $UI_ANCHO $UI_MENU_ITEMS "${opciones[@]}" 2>$VALFILE
}

function terminar
{
        rm -f $VALFILE
        exit $1
}

function cancelar
{
        informar "$(gettext "Configuración cancelada.\nNo se han realizado cambios en el sistema.")"
        terminar 1
}

function confirmar
{
        whiptail --title " $1 " --backtitle "$TITLE" --yesno "$2" $UI_ALTO $UI_ANCHO
}

function introducir
{
	whiptail --backtitle "$TITLE" --inputbox "$1" $UI_ALTO $UI_ANCHO "$2" 2>$VALFILE
}

function informar
{
	whiptail --backtitle "$TITLE" --title "$(gettext "Información")" --msgbox "$1" $UI_ALTO $UI_ANCHO
}

function mensajered
{
	whiptail --backtitle "$TITLE" --title " $1 " --msgbox "$2" $UI_ALTO $UI_ANCHO
}

################## Selección Interface de RED ##########################
#
# Obtener la configuración actual de la red
#
HOST_NAME=$(hostname)

seleccionar_interfaz || cancelar
INTERFACE=$(< $VALFILE)

IP_ADDRESS="`
  ifconfig "$INTERFACE" |
    grep "^[[:space:]]*inet\ addr" |
    sed "s/\(\ *inet\ addr:\)\([0-9]*[^ ]*\)\(.*\)/\2/g"
  `"

SUBNET="`
  route -n |
    egrep "^192\.168\.|^172\.(1[6-9]|2[0-9]|3[0-1])\.|^10\." |
    grep "$INTERFACE$" |
    head -1 |
    awk '{print $1}'
  `"

ROUTE="`route -n | grep "^0.0.0.0" | cut -b 17-32 | cut -d " " -f 1`"

BCAST_ADDRESS="`
  ifconfig "$INTERFACE" |
  grep "Bcast:" |
  sed -e "s/[^B]*.Bcast:\([0-9.]*\) .*$/\1/"
  `"

NETWORK_MASK="`
  ifconfig "$INTERFACE" |
  grep "Mask:" |
  sed -e "s/[^M]*.Mask:\([0-9.]*\).*$/\1/"
  `"
GATEWAY="`
  route -n | grep "^0.0.0.0" |
  cut -b 17-32 | cut -d " " -f 1
  `"
## Mostrar la configuración actual para la interfaz seleccionada
#mensajered "$(gettext "Valores actuales de red de la tarjeta seleccionada")" "$(eval_gettext "Nombre de servidor: \$HOST_NAME\nInterfaz: \$INTERFACE\nDirección IP: \$IP_ADDRESS\nMáscara: \$NETWORK_MASK")" 14 90 0 || cancelar
#mensajered "$(gettext "Valores que serán utilizados para la configuración.")" "$(eval_gettext "La IP: \$IP_ADDRESS será utilizada como la IP para este Servidor VozBox.")" 14 90 0 || cancelar

############################## FUNCIONES ###############################

## Funciones dialog

function pregunta
{
	dialog --title "$(gettext "Pregunta")" --backtitle "$TITLE" --yesno "$1" 14 90
}

function mensaje
{
	dialog --title "$1" --backtitle "$TITLE" --msgbox "$2" 14 90
}

function informa
{
	dialog --title "$(gettext "INFO")" --backtitle "$TITLE" --infobox "$1" 14 90
	sleep 1
	STOP=0
}

function progreso
{
(
contador=1
while [ $contador -ne 101 ] && [ "$STOP" == "0" ]
    do
	echo $contador
	echo "###"
        echo "$contador %"
        echo "###"
	((contador+=1))
	sleep 1
done
) |dialog --backtitle "$TITLE" --gauge "$1" 14 90 0
}

function selecciona
{
	dialog --inputbox "$1" 14 90 "$2" 2> /tmp/vozbox.dialog.$$
}

function progresoCORTO
{
(
contador=1
while [ $contador -ne 101 ] && [ "$STOP" == "0" ]
    do
	echo $contador
	echo "###"
        echo "$contador %"
        echo "###"
	((contador+=1))
	sleep 0.25
done
) |dialog --backtitle "$TITLE" --gauge "$1" 14 90 0
}

function lista
{
dialog --backtitle "$TITLE" \
--radiolist "$1" 14 90 40 \
		es "Spain" on \
		us "United States/North America" off \
		au "Australia" off \
		fr "France" off \
		nl "Netherlands" off \
		uk "United Kingdom" off \
		fi "Finland" off \
		jp "Japan" off \
		no "Norway" off \
		at "Austria" off \
		nz "New Zealand" off \
		it "Italy" off \
		us-old "United States Circa 1950 / North America" off \
		gr "Greece" off \
		tw "Taiwan" off \
		cl "Chile" off \
		se "Sweden" off \
		be "Belgium" off \
		sg "Singapore" off \
		il "Israel" off \
		br "Brazil" off \
		hu "Hungary" off \
		lt "Lithuania" off \
		pl "Poland" off \
		za "South Africa" off \
		pt "Portugal" off \
		ee "Estonia" off \
		mx "Mexico" off \
		in "India" off \
		de "Germany" off \
		ch "Switzerland" off \
		dk "Denmark" off \
		cz "Czech Republic" off \
		cn "China" off \
		ar "Argentina" off \
		my "Malaysia" off \
		th "Thailand" off \
		bg "Bulgaria" off \
		ve "Venezuela" off \
		ph "Philippines" off \
		ru "Russian Federation" off 2> /tmp/vozbox.dialog.$$
}

########################################################################
##################### OTRAS DEFINICIONES DE FUNCIONES ##################

# descarga_en_segundo_plano nombre fichero(s)
# Lanza en segundo plano una descarga y anota que esta pendiente
descarga_en_segundo_plano()
{
        nombre="$1"; shift
        wget --no-verbose --append-output=$WGET_LOGFILE $* >/dev/null 2>&1 &
        DESCARGAS+=([$nombre]=$!)
}

esperar_descarga()
{
        nombre="$1"; shift
        pid=${DESCARGAS[$nombre]}
        [ "$pid" != "" ] || return 1
        wait $pid
        unset "DESCARGAS[$nombre]"
}

imprimir_descargas()
{
        echo "Hay ${#DESCARGAS[@]} descargas pendientes:"
        for x in "${!DESCARGAS[@]}"
        do
                echo "    '$x' - ${DESCARGAS[$x]}"
        done
}

# esperar_descargas
# Espera hasta que acaben todas las descargas lanzadas con descarga_en_segundo_plano
esperar_descargas()
{
        imprimir_descargas
        for x in "${!DESCARGAS[@]}"
        do
                esperar_descarga "$x"
        done
}

error()
{
        echo "ERROR: $*"
        exit 1
}

# Ejemplos de uso
# Lanzar las descargas
#~ descarga_en_segundo_plano "Sofware A, B y C" ${DIRECTORIO}example-net1.pdf
#~ descarga_en_segundo_plano "Drivers para Asterisk" ${DIRECTORIO}/example-net2.pdf
#~ descarga_en_segundo_plano "Otra cosa" ${DIRECTORIO}/example-net3.pdf
#~ imprimir_descargas

# Esperar a que termine una descarga en concreto
#~ descarga="Sofware A, B y C"
#~ echo "Esperando a que acabe la descarga '$descarga'"
#~ esperar_descarga "$descarga" || error "esperando descarga"
#~ imprimir_descargas

# Esperar a que acaben todas las descargas pendientes
#~ echo "Esperando a que acaben las descargas"
#~ esperar_descargas
#~ echo "Ya está."

########################################################################

# Filtra la salida de wget y genera la salida apropiada para dialog
# parametros: paquetes a descargar
wget_dialog()
{
	# Obtener el número de paquetes a instalar
	n=$#

	i=1
	for f in $*
	do
		#
		# descargar un paquete
		#

		# cambiar el título de la etapa
		f=$(echo $f | sed -re 's/.*\/([^/]*)$/\1/')
		echo -e "XXX\n$(eval_gettext "Descargando archivo '\$f' (\$i/\$n)")\nXXX\n"

		# descartar líneas hasta que comience la descarga
		while read l
		do
			echo $l | grep -E '^[[:blank:]]*[0-9]+K[ .]*[0-9]+%.*$' >/dev/null 2>&1 && break
		done

		# procesar los porcentajes
		while read l
		do
			p=$(echo $l | sed -re 's/^[[:blank:]]*[0-9]+K[ .]*([0-9]+)%.*$/\1/')
			echo $p | grep -E '^[0-9]+' >/dev/null 2>&1 || break
			echo $p
		done
		i=$(( $i + 1 ))
		sleep 1
	done
	echo -e "XXX\n$(gettext "Descarga finalizada")\nXXX\n"
}

# interpreta la salida de wget y la muestra con dialog
# parámetros:
#	1- Título principal para el dialog gauge
#	2- Numero Paquetes a descargar
#
wget_progress()
{
	title=$1; shift;

 	wget $* 2>&1 | wget_dialog $* | dialog --backtitle "$title" --gauge "$(gettext "Descarga de archivos")" 10 80
}

# Ejemplo de uso en un script
# wget_progress "Descarga de paquetes vozbox" ruta de descarga/paquete de descarga

########################################################################

# Filtra la salida de aptitude y genera la salida apropiada para dialog
# parametros: no tiene

aptitude_dialog()
{
        local paquetes_actualizados="$(gettext -e --domain="aptitude" "%lu packages upgraded, %lu newly installed, " | sed 's/^\%lu \([^,]*\).*/\1/')"
        local necesito_descargar="$(gettext -e --domain="aptitude" "Need to get %sB/%sB of archives. " | sed 's/^\([^%]*\).*/\1/')"
        local de_archivos="$(gettext -e --domain="aptitude" "Need to get %sB/%sB of archives. " | sed -re 's/^[^%]*%sB\/%sB(.*)$/\1/')"
        local des="$(gettext -e --domain="aptitude" "Get:")"
        local descargados="$(gettext -e --domain="aptitude" "Fetched %sB in %s (%sB/s)\n" | sed 's/^\([^%]*\).*/\1/')"
        local configurando="$(gettext -e --domain="dpkg" "Setting up %s (%s) ...\n" | sed 's/^\([^%]*\).*/\1/')"
        local seleccionando_el_paquete="$(gettext -e --domain="dpkg" "Selecting previously deselected package %s.\n" | sed 's/^\([^%]*\).*/\1/')"

	# Obtener el numero de paquetes a instalar
	while read l
	do
		echo $l | grep -E "^[[:digit:]]+ ${paquetes_actualizados}" >/dev/null 2>&1 || continue
		ninst=$(echo $l | sed -re 's/^[^,]*, ([[:digit:]]+).*$/\1/')
		break
	done
	if [ ${ninst:=0} -eq 0 ]
	then
		echo -e "XXX\n$(gettext "Nada que hacer")\nXXX"
		echo "100"
		while read l; do x="x"; done
		echo -e "XXX\n$(gettext "Tarea finalizada")\nXXX"
		exit
	fi


	# Obtener el numero de bytes a descargar
	read l
	ndesc="$(echo $l | sed -re "s/^${necesito_descargar}(.*)$de_archivos.*$/\1/")"

	# Calcular el porcentaje
	n=$ninst
        nbdesc=$(echo "$ndesc" | sed -e 's/\/.*$//')
	if [ "$nbdesc" != "0 B" ]
	then
		n=$(( $n * 3 ))
	else
		n=$(( $n * 2 ))
	fi
	paso=$( echo "scale=4; 100.0 / $n.0" | bc -q )
	porcentaje=0

	# Si hay que descargar, monitorizar la descarga
	if [ "$nbdesc" != "0 B" ]
	then
		echo -e "XXX\n$(eval_gettext "Descargando ficheros (\$ndesc)")\nXXX"
		i=0
		while read l
		do
			echo $l | grep -E "^$descargados" >/dev/null 2>&1 && break
			echo $l | grep -E "^$des" >/dev/null 2>&1 || continue
			i=$((i+1))
			porcentaje=$( echo "$porcentaje + $paso" | bc -q )
			echo "$porcentaje" | sed -e 's/\..*$//'
		done
	#	echo "Descargados $i paquetes."
	fi

	# Monitorizar la instalacion
	echo -e "XXX\n$(eval_gettext "Instalando \$ninst paquetes")\nXXX"
	i=0
	while read l
	do
		echo $l | grep -E "^${configurando}" >/dev/null 2>&1 && break
		echo $l | grep -E "^${seleccionando_el_paquete}" >/dev/null 2>&1 || continue
		i=$((i+1))
		porcentaje=$( echo "$porcentaje + $paso" | bc -q )
		echo "$porcentaje" | sed -e 's/\..*$//'
	done
	# echo "Instalados $i paquetes."

	# Monitorizar la configuracion
	echo -e "XXX\n$(eval_gettext "Configurando \$ninst paquetes")\nXXX"
	i=1  # Ya se ha contado uno en el bucle anterior
	porcentaje=$( echo "$porcentaje + $paso" | bc -q )
	echo "$porcentaje" | sed -e 's/\..*$//'
	while read l
	do
		echo $l | grep -E "^${configurando}" >/dev/null 2>&1 || continue
		i=$((i+1))
		porcentaje=$( echo "$porcentaje + $paso" | bc -q )
		echo "$porcentaje" | sed -e 's/\..*$//'
	done
	porcentaje=100
	echo "$porcentaje"
	#echo "Configurados $i paquetes."
	echo -e "XXX\n$(gettext "Tarea finalizada")\nXXX"
}

# interpreta la salida de aptitude y la muestra con dialog
# parametros:
#	1- Titulo principal para el dialog gauge
#
progress_aptitude()
{
	aptitude_dialog | dialog --backtitle "$1" --gauge "$(gettext "Instalando por favor espere")" 10 80
}

# Ejemplo de uso en un script
# aptitude install -y -q paquete 2>/dev/null | progress_aptitude "Instalacion de paquete"

########################################################################
########################################################################

preseed_mysql_server ()
{
  MYSQL_SERVER_PACKAGE="`apt-cache show mysql-server | grep ^Depends | awk '{print $2}' | head -1`"
  cat << EOF | debconf-set-selections
$MYSQL_SERVER_PACKAGE mysql-server/root_password password $1
$MYSQL_SERVER_PACKAGE mysql-server/root_password_again password $1
$MYSQL_SERVER_PACKAGE mysql-server/root_password seen true
$MYSQL_SERVER_PACKAGE mysql-server/root_password_again seen true
$MYSQL_SERVER_PACKAGE mysql-server/start_on_boot boolean true
EOF

}

start_mysql_server ()
{
  /etc/init.d/mysql restart > /dev/null 2>&1
  i=0
  while [ $i -lt 20 -a ! pgrep mysqld >/dev/null 2>&1 ]
    do
      sleep 1
      (( i++ ))
    done
  pgrep mysqld >/dev/null 2>&1
}

sangoma_detected ()
{
    lspci 2>/dev/null | grep -q "Network.*Sangoma"
}

digium_detected () {
    lspci 2>/dev/null | grep -q "Digium"
}

paquete_instalado ()
{
  dpkg -l | grep -q "$1"
  #apt-cache show "$1" | grep ^Status | grep -q "install ok installed$"
}

########################### FIN DE FUNCIONES ###########################
# Llenar la caché de DNS para acelerar las descargas más tarde
for server in \
  "prdownloads.sourceforge.net/vozbox" \
  "www.deb-multimedia.org" \
  "mirror.freepbx.org" \
  "www.freepbx.org" \
  "downloads.digium.com" \
  "downloads.asterisk.org" \
  "downloads.sourceforge.net" \
  "0.debian.pool.ntp.org" \
  "1.debian.pool.ntp.org" \
  "ftp.es.debian.org" \
  "security.debian.org" \
  "ftp.debian.org" \
  "ftp.sangoma.com" \
; do
    dig +short ${server} >>/dev/null 2>>/dev/null &
    sleep 0.01 2>>/dev/null || true
done
sleep 0.5
########################################################################
#~ wget http://www.deb-multimedia.org/pool/main/d/deb-multimedia-keyring/deb-multimedia-keyring_2012.05.10-dmo4_all.deb >/dev/null 2>&1
#~ dpkg -i deb-multimedia-keyring_2012.05.10-dmo4_all.deb >/dev/null 2>&1
#~ gpg --keyserver subkeys.pgp.net --recv-keys 07DC563D1F41B907 >>"$MYLOGFILE" 2>&1 && gpg --export --armor 07DC563D1F41B907 | apt-key add - >>"$MYLOGFILE" 2>&1
apt-get update >/dev/null 2>&1
dpkg-reconfigure tzdata
dpkg-reconfigure locales
########################################################################
########################################################################
## -----------------------INICIO SCRIPT-------------------------------##
## ----------------------STARTING SCRIPT------------------------------##
########################################################################

## Bienvenida
## Welcome
main ()
{
########################### DESCARGAS SERVIDOR #########################
########################################################################
cd /usr/src
tar zxf sourceforge.tar.gz
dpkg -i deb-multimedia-keyring_2012.05.10-dmo4_all.deb >/dev/null 2>&1
descarga_en_segundo_plano "libpri" http://downloads.asterisk.org/pub/telephony/libpri/libpri-1.4-current.tar.gz -O /usr/src/libpri-1.4-current.tar.gz
descarga_en_segundo_plano "dahdi" http://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/dahdi-linux-complete-current.tar.gz -O /usr/src/dahdi-linux-complete-current.tar.gz
#~ descarga_en_segundo_plano "asterisk" http://downloads.asterisk.org/pub/telephony/certified-asterisk/${VER_ASTERISK_CERT} -O /usr/src/asterisk-current.tar.gz
#descarga_en_segundo_plano "webmin" http://prdownloads.sourceforge.net/webadmin/webmin-${VER_WEBMIN}-minimal.tar.gz -O /usr/src/webmin-${VER_WEBMIN}-minimal.tar.gz
#~ descarga_en_segundo_plano "spandsp" http://www.soft-switch.org/downloads/spandsp/spandsp-${VER_SPAN}pre21.tgz
apt-get update >/dev/null 2>&1
########################### DESCARGAS SERVIDOR #########################
########################################################################

#whiptail --title "$(gettext "'INSTALACIÓN DE SISTEMA DE COMUNICACIONES IP VOZBOX'")" --msgbox "$(gettext "El uso de este SCRIPT está supeditado a la aceptación de los términos de la licencia.\nCopyright Daver Jorge 2012 <davervozbox@gmail.com>\nEste script está bajo la licencia GNU GPL version 3.0.\nPara ver una copia de la licencia visite la web: http://www.gnu.org/copyleft/gpl.html\nEste Script NO GARANTIZA EXPRESAMENTE EL BUEN FUNCIONAMIENTO DE SU SISTEMA Y POR TANTO, SU USO EN SISTEMAS EN PRODUCCIÓN, ES RESPONSABILIDAD TOTAL DEL INSTALADOR.")" 24 80 0
#whiptail --title "$(gettext "'INSTALACIÓN DE SISTEMA DE COMUNICACIONES IP VOZBOX'")" --msgbox "$(eval_gettext "A continuación se iniciará la instalación del sistema VozBox \$VER_VERSION \n\nPARTE 1. INSTALACIÓN DE Dependencias. \nPARTE 2. INSTALACIÓN DE Asterisk. \nPARTE 3. INSTALACIÓN DE FreePBX-VozBox.")" 24 80 0

echo "PARTE 1. INSTALACIÓN DE DEPENDENCIAS" >>"$MYLOGFILE" 2>&1

echo "" >> /root/passwords
echo "################ $(gettext "INICIO VOZBOX PASSWORDS") ###################" >> /root/passwords
echo "# MYSQL ROOT PASSWORD" >> /root/passwords
echo "MYSQL_ROOT_PW=\"${MYSQL_ROOT_PW}\";" >> /root/passwords
echo "" >> /root/passwords
echo "# ASTERISK PASSWORDS" >> /root/passwords
echo "ASTERISK_DB=\"${ASTERISK_DB}\";" >> /root/passwords
echo "ASTERISK_DB_USER=\"${ASTERISK_DB_USER}\";" >> /root/passwords
echo "ASTERISK_DB_PW=\"${ASTERISK_DB_PW}\";" >> /root/passwords
echo "" >> /root/passwords
echo "# ASTERISK MANAGER INTERFACE (AMI)" >> /root/passwords
echo "AMPMGR_USER=\"${AMPMGR_USER}\";" >> /root/passwords
echo "ASTERISK_MGR_PW=\"${ASTERISK_MGR_PW}\";" >> /root/passwords
echo "" >> /root/passwords
echo "# ARI PASSWORDS" >> /root/passwords
echo "ARI_ADMIN_USERNAME=\"${ARI_ADMIN_USERNAME}\";" >> /root/passwords
echo "ARI_ADMIN_PASSWORD=\"${ARI_PW}\";" >> /root/passwords
echo "" >> /root/passwords
echo "# FREEPBX WEB GUI" >> /root/passwords
echo "FREEPBX_USER_GUI=\"${FREEPBX_USER_GUI}\";" >> /root/passwords
echo "VER_FREEPBX=\"${VER_FREEPBX}\";" >> /root/passwords
echo "" >> /root/passwords
echo "# FLASH OPERATOR PANEL2" >> /root/passwords
echo "FOP2_PW=\"${FOP2_PW}\";" >> /root/passwords
echo "" >> /root/passwords
echo "# VTIGER_DB_PW" >> /root/passwords
echo "VTIGER_DB_PW=\"${VTIGER_DB_PW}\";" >> /root/passwords

aptitude install -y -q ${PAQUETES} 2>/dev/null | progress_aptitude "$(gettext "Instalando paquetes y dependencias ... Por favor espere ...")"
STOP=1
informa "$(gettext "Paquetes y dependencias Instaladas.")"

aptitude install -y -q linux-source-${KERNEL_VERSION} kernel-package linux-headers-${KERNEL_VERSION2} 2>/dev/null | progress_aptitude "$(eval_gettext "Instalando kernel headers para \${KERNEL_VERSION}... Por favor espere ...")"
ln -s /usr/src/linux-headers-${KERNEL_VERSION2} /usr/src/linux

#Instalar las dependencias de Google Voice
#~ cd /usr/src
#~ wget https://iksemel.googlecode.com/files/iksemel-1.4.tar.gz
#~ tar xf iksemel-1.4.tar.gz
#~ cd iksemel-1.4
#~ ./configure
#~ make
#~ make install

echo "$(gettext "INSTALACIÓN DE POSTFIX")" >>"$MYLOGFILE" 2>&1
aptitude install -y postfix

####-------------------extraer paquetes vozbox---------------------#####

cd /usr/src
tar zxf paq_tarjetas.tar.gz
tar zxf paq_webmin.tar.gz
tar zxf paq_extras.tar.gz
rm -rf paq_webmin.tar.gz
rm -rf paq_extras.tar.gz
rm -rf paq_tarjetas.tar.gz

###------------CREACION DE FICHERO AYUDA VOZBOX-HELP-----------------###
mv /usr/src/vozbox-* /usr/local/bin/
mv /usr/src/dropiptables /usr/local/bin/dropiptables
mv /usr/src/testdevelocidad.py /usr/local/bin/testdevelocidad.py
mv /usr/src/asterisk.nanorc /usr/share/nano/asterisk.nanorc

chmod +x /usr/local/bin/vozbox-*
chmod +x /usr/local/bin/dropiptables
chmod +x /usr/local/bin/testdevelocidad.py

#echo "$(gettext "GENERANDO MENU DE INICIO")" >>"$MYLOGFILE" 2>&1
#progresoCORTO "$(gettext "GENERANDO MENU DE INICIO...")"
#if [ -f /etc/debian_version -a "$(cat /etc/debian_version | cut -d. -f1)" -ge 6 ]
#then
#	cp -a /etc/default/grub /etc/default/grub-orig
#	sed -i '9i\GRUB_BACKGROUND=\/boot\/grub\/images\/grub3.png' /etc/default/grub
#	sed -i '/^#/d' /etc/default/grub
#	sed -i '/^$/d' /etc/default/grub
#	mkdir /boot/grub/images
#	mv /usr/src/grub3.png /boot/grub/images
# 	update-grub >>"$MYLOGFILE" 2>&1
#else
#	cp /boot/grub/menu.lst /boot/grub/menu.lst-orig
#	sed -i '23i\splashimage=(hd0,0)\/boot\/grub\/images\/grub3.xpm.gz' /boot/grub/menu.lst
#	sed -i '/^#/d' /boot/grub/menu.lst
#	sed -i '/^$/d' /boot/grub/menu.lst
#	mkdir /boot/grub/images
#	mv /usr/src/grub3.xpm.gz /boot/grub/images
#fi
#STOP=1
#informa "$(gettext "Terminada la edición de grub")"

###----------------Instalando multimedia y lame---------------------####

cd /usr/src/
progresoCORTO "$(gettext "INSTALANDO lame...")"
apt-get install -y --force-yes lame >>"$MYLOGFILE" 2>&1
#~ aptitude install -y lame >>"$MYLOGFILE" 2>&1
STOP=1
informa "$(gettext "debian-multimedia-keyring y lame instalados")"

####-------------------Creamos Directorio TFTP-----------------------####
#
#informa "$(gettext "CREANDO DIRECTORIO SERVIDOR TFTP...")"
#cd /usr/src
#mkdir //tftpboot
#tar zxf paq_aastra.tar.gz -C /tftpboot/
#cp /tftpboot/setup-aastra-xml /usr/local/bin/vozbox-aastra-xml
#mv /tftpboot/setup-aastra-xml /usr/local/sbin/setup-aastra-xml
#chmod +x /usr/local/bin/vozbox-aastra-xml
#chmod 777 -R /tftpboot
#rm -rf paq_aastra.tar.gz
#
####-------------Preparando para Instalación de sangoma---------------###
#if sangoma_detected
#  then
#  cd /usr/src
#  echo "$(gettext "Detectada Tarjeta Sangoma")" >>"$MYLOGFILE" 2>&1
#  informa "$(gettext "Determinando y preparando versión Sangoma ...")"
#  wget_progress "$(gettext "Descargando Sangoma")"  $SANGOMA_TARBALLS -O /usr/src/wanpipe-current.tgz
#  aptitude install -y -q $SANGOMA_BUILD_DEPENDS 2>/dev/null | progress_aptitude "$(eval_gettext "Instalando \$SANGOMA_BUILD_DEPENDS ...")"
#fi

#mensaje "$(gettext "PROGRESO DE LA INSTALACIÓN")" "############## $(gettext "PARTE 2 - INSTALACIÓN DE ASTERISK  ##############")" 14 90 0
#echo "############## $(gettext "PARTE 2 - INSTALACIÓN DE ASTERISK  ##############")" >>"$MYLOGFILE" 2>&1

####---------------------Instalando Libpri-------------------------#####

echo "$(gettext "INSTALANDO LIBPRI")" >>"$MYLOGFILE" 2>&1
cd /usr/src
descarga="libpri"
esperar_descarga "$descarga" || error "$(gettext "esperando descarga")"
progreso "$(eval_gettext "INSTALANDO libpri-\${VER_LIBPRI}...")"
tar zxf /usr/src/libpri-1.4-current.tar.gz
cd /usr/src/libpri-${VER_LIBPRI}
make >>"$MYLOGFILE" 2>&1
make install >>"$MYLOGFILE" 2>&1
rm -rf /usr/src/libpri-1.4-current.tar.gz
STOP=1
informa "$(eval_gettext "libpri-\${VER_LIBPRI} instalado.")"

####---------------------Instalando Dahdi--------------------------#####

echo "$(gettext "INSTALANDO DAHDI-LINUX")" >>"$MYLOGFILE" 2>&1
cd /usr/src
descarga="dahdi"
esperar_descarga "$descarga" || error "$(gettext "esperando descarga")"
progreso "$(eval_gettext "INSTALANDO DAHDI-LINUX-COMPLETE...")"
tar zxf /usr/src/dahdi-linux-complete-current.tar.gz
ln -s "/usr/src/`tar -tf "/usr/src/dahdi-linux-complete-current.tar.gz" | head -1`" /usr/src/dahdi
cd /usr/src/dahdi
make > /dev/null 2>&1
make install > /dev/null 2>&1
make config > /dev/null 2>&1
STOP=1
rm -rf /usr/src/dahdi-linux-complete-current.tar.gz
informa "$(eval_gettext "dahdi-\${VER_DAHDI_LINUX_COMPLETE} instalado.")"

### Instalando Spandsp
echo "INSTALANDO SPANDSP" >>"$MYLOGFILE" 2>&1
cd /usr/src
progreso "INSTALANDO SPANDSP-${VER_SPAN}..."
tar zxf spandsp-${VER_SPAN}pre21.tgz
cd spandsp-${VER_SPAN}
chmod +x configure
./configure --prefix=/usr/local >>"$MYLOGFILE" 2>&1
make >>"$MYLOGFILE" 2>&1
make install >>"$MYLOGFILE" 2>&1
ldconfig
STOP=1
informa "Spandsp instalado."

####---------------------Instalando Asterisk------------------------#####

echo "$(gettext "INSTALANDO Asterisk")" >>"$MYLOGFILE" 2>&1
cd /usr/src
svn co http://svn.digium.com/svn/asterisk/certified/branches/${BRANCH_ASTERISK} asterisk >>"$MYLOGFILE" 2>&1
#~ descarga="asterisk"
#~ esperar_descarga "$descarga" || error "$(gettext "esperando descarga")"
#~ tar zxf /usr/src/asterisk-current.tar.gz
#~ ln -s "/usr/src/`tar -tf "/usr/src/asterisk-current.tar.gz" | head -1`" /usr/src/asterisk
cd /usr/src/asterisk

###-----modificando safe asterisk script para utilizar con Debian----###

sed -i 's/#!\/bin\/sh/\#!\/bin\/bash/g' "/usr/src/asterisk/contrib/scripts/safe_asterisk"
./contrib/scripts/install_prereq install
./contrib/scripts/get_mp3_source.sh >>"$MYLOGFILE" 2>&1
./configure >>"$MYLOGFILE" 2>&1

# Pre-configuración de Asterisk
make menuselect.makeopts >&2
make makeopts >&2
eval "menuselect/menuselect $OPCIONES_MENUSELECT"

progreso "$(gettext "INSTALANDO Asterisk...")"
make >>"$MYLOGFILE" 2>&1
make install >>"$MYLOGFILE" 2>&1
make samples >>"$MYLOGFILE" 2>&1
make config >>"$MYLOGFILE" 2>&1
STOP=1
informa "$(gettext "Asterisk instalado.")"

####---------------------Instalando Voces--------------------------#####

echo "$(gettext "INSTALANDO Voces del Sistema")" >>"$MYLOGFILE" 2>&1
# Instale Asterisk-extra-Sonidos
cd /var/lib/asterisk/sounds
wget http://downloads.asterisk.org/pub/telephony/sounds/asterisk-extra-sounds-en-gsm-current.tar.gz >>"$MYLOGFILE" 2>&1
tar zxf asterisk-extra-sounds-en-gsm-current.tar.gz
rm asterisk-extra-sounds-en-gsm-current.tar.gz
cd /usr/src/
tar zxf voces.tar.gz
rm -rf voces.tar.gz
cd voces
mv * /var/lib/asterisk/sounds/
cd ..
rm -R /usr/src/voces
informa "$(gettext "Voces del sistema instaladas.")"

#mensaje "$(gettext "VozBox. PROGRESO DE LA INSTALACIÓN")" "##############  $(gettext "PARTE 3 - Instalación de FreePBX-VozBox")  ##############" 14 90 0
#echo "##############  $(gettext "PARTE 3 - Instalación de FreePBX-VozBox")  ##############" >>"$MYLOGFILE" 2>&1

# Descargar y extraer FreePBX
cd /usr/src
svn co http://www.freepbx.org/v2/svn/freepbx/branches/${VER_FREEPBX} freepbx >>"$MYLOGFILE" 2>&1

###--------------Creamos el usuario y grupo asterisk-----------------###

informa "$(gettext "GENERANDO USUARIOS...")"
groupadd asterisk
useradd -c "PBX VozBox" -d /var/lib/asterisk -g asterisk asterisk
chown -R asterisk:asterisk /var/run/asterisk

###----------Autoprovisionamiento para terminales Aastra-------------###

echo "$(gettext "Autoprovisionamiento para terminales Aastra 675xi")" >>"$MYLOGFILE" 2>&1
cd /usr/src/
informa "$(gettext "Autoprovisionamiento para terminales Aastra 675xi")"
tar zxf aastra-xml-2.3.1.tar.gz -C / --no-same-owner --no-same-permissions --no-overwrite-dir
rm -rf aastra-xml-2.3.1.tar.gz
chmod 770 -R /var/www/aastra
chown -R asterisk:asterisk /var/www/aastra
chown asterisk:asterisk /var/cache/aastra
chmod 770 /var/cache/aastra
chown asterisk:asterisk /usr/local/sbin/setup-aastra-xml
chmod 744 /usr/local/sbin/setup-aastra-xml
chmod +x /usr/local/bin/vozbox-aastra-xml
chown asterisk:asterisk /etc/asterisk/extensions_aastra.conf
mv /tftpboot/contacts.txt /var/cache/aastra/contacts.txt
chown asterisk:asterisk /var/lib/asterisk/agi-bin
chmod o-w /var/lib/asterisk/agi-bin
chmod +X /var/lib/asterisk/agi-bin/*
chown -R asterisk:asterisk /var/lib/asterisk/sounds/custom
chmod 775 /var/lib/asterisk/sounds/custom
sed -i "s/\(^password=*\)\(.*\)/password=${ASTERISK_MGR_PW}/" /var/www/aastra/config/asterisk.conf

###----------Instalación de paquetes y dependencias LAMP-------------###

informa "$(gettext "INSTALANDO DEPENDENCIAS Y PAQUETES LAMP")"
echo "$(gettext "INSTALANDO DEPENDENCIAS Y PAQUETES LAMP")" >>"$MYLOGFILE" 2>&1
# Instalar mysql-server
echo "$(gettext "Instalando mysql server")" >>"$MYLOGFILE" 2>&1
preseed_mysql_server ${MYSQL_ROOT_PW} MYSQL_SERVER_PACKAGE
informa "$(gettext "mysql-server instalado.")"
aptitude install -y -q apache2-mpm-prefork apache2-utils php5-mysql mysql-server mysql-common unixodbc unixodbc-dev libmyodbc mysql-client 2>/dev/null | progress_aptitude "$(gettext "INSTALANDO dependencias y paquetes lamp ...")"
informa "$(gettext "Dependencias y paquetes lamp instalados.")"

###-------------------Creación de las bases de datos-----------------###

informa "$(gettext "GENERANDO BASES DE DATOS...")"
echo "CREATE DATABASE IF NOT EXISTS asteriskcdrdb;" | mysql -u root -p${MYSQL_ROOT_PW}
echo "CREATE DATABASE IF NOT EXISTS asterisk;" | mysql -u root -p${MYSQL_ROOT_PW}

###--------Creación de los usuarios de la Base de Datos (BD)---------###

informa "$(gettext "GENERANDO USUARIOS BASES DE DATOS...")"
echo "GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO ${ASTERISK_DB_USER}@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "GRANT ALL PRIVILEGES ON asterisk.* TO ${ASTERISK_DB_USER}@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "flush privileges;" | mysql -u root -p${MYSQL_ROOT_PW}

###------------------Instalando FreePBX-VozBox-----------------------###

#echo "$(gettext "Instalando FreePBX-VozBox")" >>"$MYLOGFILE" 2>&1
echo "$(gettext "Instalando FreePBX")" >>"$MYLOGFILE" 2>&1
cd /usr/src
# Instalar PearDB
aptitude install -y -q php-pear 2>/dev/null | progress_aptitude "$(gettext "INSTALANDO php-pear ...")"
pear install db > /dev/null 2>&1
cd /usr/src/freepbx
mysql -u${ASTERISK_DB_USER} -p${ASTERISK_DB_PW} asteriskcdrdb < /usr/src/freepbx/SQL/cdr_mysql_table.sql
mysql -u${ASTERISK_DB_USER} -p${ASTERISK_DB_PW} asterisk < /usr/src/freepbx/SQL/newinstall.sql

###-----------Cambiamos el usuario propietario de Apache-------------###

cp /etc/group /etc/group-orig
sed -i "s/\(^www-data:x:33: *\)/\1asterisk/" /etc/group

###--------Cambiar el usuario y grupo de www-data a Asterisk---------###

cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf-orig
sed -i "s/\(^User *\)\(.*\)/\1asterisk/" /etc/apache2/apache2.conf
sed -i "s/\(^Group *\)\(.*\)/\1asterisk/" /etc/apache2/apache2.conf
sed -i "s/#AddDefaultCharset UTF-8/AddDefaultCharset UTF-8/" /etc/apache2/conf.d/charset
apache2ctl restart > /dev/null 2>&1

sed -i "s/\(export APACHE_RUN_USER *= *\)\(.*\)/\1asterisk/" /etc/apache2/envvars
sed -i "s/\(export APACHE_RUN_GROUP *= *\)\(.*\)/\1asterisk/" /etc/apache2/envvars

###---------Edición php aumento del tamaño de archivos web-----------###

cp /etc/php5/apache2/php.ini /etc/php5/apache2/php.ini-orig
sed -i "s/\(upload_max_filesize *= *\)\(.*\)/\140M/" /etc/php5/apache2/php.ini
sed -i "s/\(max_execution_time *= *\)\(.*\)/\1120/" /etc/php5/apache2/php.ini
sed -i "s/\(max_input_time *= *\)\(.*\)/\1120/" /etc/php5/apache2/php.ini
sed -i "s/\(memory_limit *= *\)\(.*\)/\1100M/" /etc/php5/apache2/php.ini
sed -i "s/\(magic_quotes_gpc *= *\)\(.*\)/\1Off/" /etc/php5/apache2/php.ini
# echo "extension=mysql.so" >> /etc/php5/apache2/php.ini
chown -R asterisk:asterisk /var/lock/apache2/

###---------------Reiniciamos el servidor web apache-----------------###

service apache2 restart > /dev/null 2>&1

###---------Editamos el archivo de configuración de asterisk---------###

cp /etc/asterisk/asterisk.conf /etc/asterisk/asterisk.conf-orig
sed -i "s/\(astrundir *=> *\)\(.*\)/\1\/var\/run\/asterisk/" /etc/asterisk/asterisk.conf

###-----Permisos de usuario y grupo a directorios de asterisk--------###

chown asterisk:asterisk /var/run/asterisk
chown -R asterisk:asterisk /etc/asterisk/
chown -R asterisk:asterisk /var/{lib,log,spool}/asterisk
chown -R asterisk:asterisk /usr/lib/asterisk
chmod 770 /var/lib/asterisk/
chmod -R 775 /var/lib/asterisk/sounds/
chmod -R 770 /var/spool/asterisk/
ln -s /var/lib/asterisk/moh /var/lib/asterisk/mohmp3
mkdir ${AMP_WEB}
chown -R asterisk:asterisk ${AMP_WEB}
chmod 755 /usr/src/freepbx/apply_conf.sh

###------------Cambiando usuario y puerto SSH------------------------###

mensaje "$(gettext "$TITLE")" "##############  $(gettext "Crear un usuario para conectar por ssh al Sistema.")  ##############" 14 90 0

selecciona "$(gettext "Entre nuevo puerto ssh:")" $port
port=$(cat /tmp/vozbox.dialog.$$)
if [ -z "$port" ]; then
port="22";
fi
selecciona "$(gettext "Entre nombre de usuario:")" $username
username=$(cat /tmp/vozbox.dialog.$$)
selecciona "$(gettext "Entre password:")" $password
password=$(cat /tmp/vozbox.dialog.$$)
grep "^$username" /etc/passwd >/dev/null

if [ $? -eq 0 ]; then
	informa "$(eval_gettext "El usuario \$username ya existe!")"
else
	pass=$(perl -e 'print crypt($ARGV[0], "password")' "$password")
	useradd $username -m -p $pass
	echo "$username   ALL=(ALL) ALL" >> /etc/sudoers
	sed -i "s/\(^Port *\)\(.*\)/\1$port/" /etc/ssh/sshd_config
	sed -i "s/\(^PermitRootLogin *\)\(.*\)/\1no/" /etc/ssh/sshd_config
	service ssh restart >/dev/null
	[ $? -eq 0 ] && informa "$(gettext "El usuario ha sido agregado al Sistema!")" || informa "$(gettext "Fallo al agregar el usuario!")"
fi

whiptail --title "$TITLE" --msgbox "$(eval_gettext "Cambios aplicados correctamente.\nRecuerde que podrá acceder al sistema por SSH con usuario: \$username.\nUna vez dentro podrá obtener permisos root con los comandos:\nsudo -s\n o\nsu -")" 14 80 0
whiptail --title "$TITLE" --msgbox "$(gettext "Recuerde que el usuario root ya no está disponible para conectar al sistema por SSH.\n\nSi desea conectar con usuario root debe editar el fichero de configuración /etc/ssh/sshd_config y cambiar el valor de la cadena PermitRootLogin")" 14 80 0
rm /tmp/vozbox.dialog.*
#PUERTO SSH
PUERTOSSH=$(cat /etc/ssh/sshd_config | grep '^Port' | grep -o '[0-9]*$');

###------------Asegurarse que asterisk crontab existe----------------###

CRONTAB="/var/spool/cron/crontabs/asterisk"
touch "${CRONTAB}"
chown asterisk:asterisk ${CRONTAB}
chmod 600 ${CRONTAB}

###------------------Disable TTY9 for OpenVZ-------------------------###

sed -i 's/TTY=9/#TTY=9/g'  /usr/sbin/safe_asterisk

#--------------Iniciar Asterisk para instalación de FreePBX----------###

STOP=1
informa "$(gettext "Iniciando ASTERISK...")"
/usr/sbin/asterisk
arr="/var/run/asterisk/asterisk.pid"
until [ -e $arr ] ; do
	echo -n "."
	sleep 0.5
done

cp /usr/src/freepbx/install_amp /usr/src/freepbx/install_amp.bkp
perl -pi -e "s,/var/www/html,${AMP_WEB},g" /usr/src/freepbx/install_amp
sed -i "s,\(^\$webroot*\)\(.*\),\1 = \"${AMP_WEB}\";," /usr/src/freepbx/install_amp
sed -i "s/xx.xx.xx.xx/${IP_ADDRESS}/g" "/usr/src/freepbx/install_amp"
chmod 755 /usr/src/freepbx/install_amp
chmod 755 /usr/src/freepbx/amp_conf/bin/gen_amp_conf.php

#---------------------------Editamos amportal------------------------###

informa "<<<<<<<<<<  $(gettext "CONFIGURANDO AMPORTAL")  >>>>>>>>>>>"
sed -i "s/\(AMPDBHOST= *\)\(.*\)/\1localhost/" amportal.conf
sed -i "s/\(AMPMGRUSER= *\)\(.*\)/\1${AMPMGR_USER}/" amportal.conf
sed -i "s/\(AMPMGRPASS= *\)\(.*\)/\1${ASTERISK_MGR_PW}/" amportal.conf
sed -i "s,\(^\AMPWEBROOT*\)\(.*\),\1 = ${AMP_WEB}," amportal.conf
sed -i "s,\(^\FOPWEBROOT*\)\(.*\),\1 = ${FOP_WEB}," amportal.conf
sed -i "s/\(FOPPASSWORD= *\)\(.*\)/\1${FOP2_PW}/" amportal.conf
sed -i "s/\(ARI_ADMIN_USERNAME= *\)\(.*\)/\1${ARI_ADMIN_USERNAME}/" amportal.conf
sed -i "s/\(ARI_ADMIN_PASSWORD= *\)\(.*\)/\1${ARI_PW}/" amportal.conf
sed -i "s/\(AUTHTYPE *= *\)\(.*\)/\1database/" amportal.conf

echo "AMPWEBADDRESS=${IP_ADDRESS}" >> amportal.conf
echo "AMPDBUSER=${ASTERISK_DB_USER}" >> amportal.conf
echo "AMPDBPASS=${ASTERISK_DB_PW}" >> amportal.conf
echo "AMPDBNAME=asterisk" >> amportal.conf
echo "ASTETCDIR=/etc/asterisk" >> amportal.conf
echo "ASTMODDIR=/usr/lib/asterisk/modules" >> amportal.conf
echo "ASTVARLIBDIR=/var/lib/asterisk" >> amportal.conf
echo "ASTAGIDIR=/var/lib/asterisk/agi-bin" >> amportal.conf
echo "ASTSPOOLDIR=/var/spool/asterisk" >> amportal.conf
echo "ASTRUNDIR=/var/run/asterisk" >> amportal.conf
echo "ASTLOGDIR=/var/log/asterisk" >> amportal.conf
echo "SSHPORT=${PUERTOSSH}" >> amportal.conf
echo "USE_FREEPBX_MENU_CONF=TRUE" >> amportal.conf

cp /usr/src/freepbx/amportal.conf /etc/amportal.conf
chown asterisk:asterisk /etc/amportal.conf
chmod 777 /etc/amportal.conf

########################################################################

progreso "$(eval_gettext "Instalado FreePBX-\${VER_FREEPBX}...")"
./install_amp --username=${ASTERISK_DB_USER} --password=${ASTERISK_DB_PW} >>"$MYLOGFILE" 2>&1

###----------Creamos /etc/default/asterisk y lo rellenamos:----------###

cat >/etc/default/asterisk << fin
RUNASTERISK=yes
AST_USER=asterisk
AST_GROUP=asterisk
fin
STOP=1
informa "$(eval_gettext "FreePBX-\${VER_FREEPBX} instalado.")"
sleep 0.5

###-------Realizamos algunos cambios para el FreePBX-VozBox----------###

progreso "$(eval_gettext "CONFIGURANDO FreePBX-\${VER_FREEPBX}--Parte-1...")"

###-------------cambio password asterisk manager---------------------###

cp /etc/asterisk/manager.conf /etc/asterisk/manager.conf-orig
sed -i "s/\(^secret = *\)\(.*\)/secret = ${ASTERISK_MGR_PW}/" /etc/asterisk/manager.conf
sed -i "s/\(^user = *\)\(.*\)/user = ${ASTERISK_DB_USER}/" /etc/asterisk/cdr_mysql.conf
sed -i "s/\(^password = *\)\(.*\)/password = ${ASTERISK_DB_PW}/" /etc/asterisk/cdr_mysql.conf

###-------conexiones exclusivamente desde la red corporativa---------###
sed -i "s/\(^bindaddr = *\)\(.*\)/bindaddr = 127.0.0.1/" /etc/asterisk/manager.conf

###-------------------setup aastra-xml manager-----------------------###

cat > /etc/asterisk/manager_custom.conf <<ENDLINE
[aastra-xml]
secret = ${ASTERISK_MGR_PW}
deny = 0.0.0.0/0.0.0.0
permit = 127.0.0.1/255.255.255.0
read = system,call,log,verbose,command,agent,user,config,command,dtmf,reporting,cdr,dialplan,originate
write = system,call,log,verbose,command,agent,user,config,command,dtmf,reporting,cdr,dialplan,originate
writetimeout = 5000
ENDLINE

###---------------RECONFIGURANDO ARI USER AND PASSWORD---------------###

sed -i "s/\(ARI_ADMIN_USERNAME= *\)\(.*\)/\1${ARI_ADMIN_USERNAME}/" /etc/amportal.conf
echo "UPDATE \`${ASTERISK_DB}\`.\`freepbx_settings\` SET \`value\` = '${ARI_ADMIN_USERNAME}' WHERE \`freepbx_settings\`.\`keyword\` = 'ARI_ADMIN_USERNAME';" | mysql -u root -p${MYSQL_ROOT_PW}
sed -i "s/\(ARI_ADMIN_PASSWORD= *\)\(.*\)/\1${ARI_PW}/" /etc/amportal.conf
echo "UPDATE \`${ASTERISK_DB}\`.\`freepbx_settings\` SET \`value\` = '${ARI_PW}' WHERE \`freepbx_settings\`.\`keyword\` = 'ARI_ADMIN_PASSWORD';" | mysql -u root -p${MYSQL_ROOT_PW}
STOP=1
informa "$(eval_gettext "FreePBX-\${VER_FREEPBX}--Parte-1 configurado.")"
sleep 0.5
progreso "$(eval_gettext "CONFIGURANDO FreePBX-\${VER_FREEPBX}--Parte-2...")"

###----------Instalando y actualizando módulos freepbx---------------###

amportal a ma installall >>"$MYLOGFILE" 2>&1
amportal a reload >>"$MYLOGFILE" 2>&1

###-----------------Eliminando módulos adicionales-------------------###

amportal a ma uninstall sipstation >>"$MYLOGFILE" 2>&1
amportal a ma uninstall blacklist >>"$MYLOGFILE" 2>&1
amportal a ma uninstall pinsets >>"$MYLOGFILE" 2>&1
amportal a ma uninstall setcid >>"$MYLOGFILE" 2>&1
amportal a ma uninstall callrecording >>"$MYLOGFILE" 2>&1
amportal a ma uninstall campon >>"$MYLOGFILE" 2>&1
amportal a ma uninstall customappsreg >>"$MYLOGFILE" 2>&1
amportal a ma uninstall hotelwakeup >>"$MYLOGFILE" 2>&1
amportal a ma uninstall digiumaddoninstaller >>"$MYLOGFILE" 2>&1
amportal a ma uninstall paging >>"$MYLOGFILE" 2>&1
amportal a ma uninstall userpaneltab >>"$MYLOGFILE" 2>&1
amportal a reload >>"$MYLOGFILE" 2>&1
service apache2 restart > /dev/null 2>&1

###----------------------MySQL config ODBC---------------------------###
if [ "${CPUINFO}" == "i686" ]; then
	 ODBCCPUINFO="i386";
	else
	 ODBCCPUINFO="x86_64";
fi
cat > /etc/odbcinst.ini <<ENDLINE
[MySQL]
Description = ODBC for MySQL
Driver = /usr/lib/${ODBCCPUINFO}-linux-gnu/odbc/libmyodbc.so
Setup = /usr/lib/${ODBCCPUINFO}-linux-gnu/odbc/libodbcmyS.so
FileUsage = 1
ENDLINE

cat > /etc/odbc.ini <<ENDLINE
[MySQL-asteriskcdrdb]
Description     = MySQL ODBC Driver
Driver          = MySQL
Socket          = /var/run/mysqld/mysqld.sock
Server          = localhost
Database        = asteriskcdrdb
Option          = 3
ENDLINE

###-----------------DELETE NOTIFICATIONS FIX-------------------------###

mysql -u root -p${MYSQL_ROOT_PW} -e "delete from notifications" asterisk
/usr/local/sbin/amportal restart > /dev/null 2>&1
STOP=1
informa "$(eval_gettext "FreePBX-\${VER_FREEPBX}--Parte-2 configurado.")"
sleep 0.5
progreso "$(eval_gettext "CONFIGURANDO FreePBX-\${VER_FREEPBX}--Finalizando...")"

###---------------------Configurando freepbx-------------------------###

echo "use asterisk; update freepbx_settings set value = '${AMPMGR_USER}' where keyword = 'AMPMGRUSER';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update freepbx_settings set value = '${ASTERISK_MGR_PW}' where keyword = 'AMPMGRPASS';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update freepbx_settings set value = '24 Hour Format' where keyword = 'TIMEFORMAT';" | mysql -u root -p${MYSQL_ROOT_PW}

echo "use asterisk; INSERT INTO sipsettings VALUES ('nat','no','','');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO sipsettings VALUES ('externip','nat_mode','','');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO sipsettings VALUES ('externip_val','${IP_ADDRESS}','','');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO sipsettings VALUES ('localnet_0','${SUBNET}','','');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO sipsettings VALUES ('netmask_0','${NETWORK_MASK}','','');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO sipsettings VALUES ('allowguest','no','','');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO sipsettings VALUES ('alwaysauthreject','yes','','9');" | mysql -u root -p${MYSQL_ROOT_PW}

echo "use asterisk; update sipsettings set data = '10001' where keyword = 'rtpstart';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update sipsettings set data = '1' where keyword = 'alaw';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update sipsettings set data = '2' where keyword = 'g722';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update sipsettings set data = '3' where keyword = 'ulaw';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update sipsettings set data = '60' where keyword = 'rtptimeout';" | mysql -u root -p${MYSQL_ROOT_PW}

echo "use asterisk; update iaxsettings set data = '1' where keyword = 'alaw';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update iaxsettings set data = '2' where keyword = 'g722';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; update iaxsettings set data = '3' where keyword = 'ulaw';" | mysql -u root -p${MYSQL_ROOT_PW}

echo "use asterisk; INSERT INTO outroutemsg VALUES ('default_msg_id','-2');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO outroutemsg VALUES ('intracompany_msg_id','-2');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO outroutemsg VALUES ('emergency_msg_id','-2');" | mysql -u root -p${MYSQL_ROOT_PW}

/usr/src/freepbx/apply_conf.sh >>"$MYLOGFILE" 2>&1
amportal a ma upgrade framework >>"$MYLOGFILE" 2>&1
amportal a ma upgrade fw_ari >>"$MYLOGFILE" 2>&1
STOP=1
informa "$(eval_gettext "Configuración FreePBX-\${VER_FREEPBX} realizada.")"
sleep 0.5

###---------------Creando enlaces simbólicos FreePBX-----------------###

cd /etc/asterisk
rm confbridge.conf features.conf sip.conf iax.conf logger.conf extensions.conf sip_notify.conf cel.conf cel_odbc.conf udptl.conf http.conf
ln -s ${AMP_WEB}/admin/modules/core/etc/* /etc/asterisk/
ln -s ${AMP_WEB}/admin/modules/cdr/etc/* /etc/asterisk/
chown -R asterisk:asterisk ${AMP_WEB}
/usr/src/freepbx/apply_conf.sh >>"$MYLOGFILE" 2>&1
sed -i "s/\(format=*\)\(.*\)/\1wav/" /etc/asterisk/vm_general.inc

###-----------Instalación del Panel Operador Flash 2-----------------###

echo "$(gettext "Instalando Panel Operador Flash 2")" >>"$MYLOGFILE" 2>&1
cd /usr/src
if [ "${CPUINFO}" == "i686" ]; then
	 wget http://download.fop2.com/fop2-${VER_FOP2}-debian-i386.tgz -O /usr/src/fop2-${VER_FOP2}.tgz
	else
	 wget http://download.fop2.com/fop2-${VER_FOP2}-debian-x86_64.tgz -O /usr/src/fop2-${VER_FOP2}.tgz
fi

#INSTALACIÓN
progresoCORTO "$(gettext "Instalando Panel Operador Flash 2")"
tar zxf fop2-${VER_FOP2}.tgz >>"$MYLOGFILE" 2>&1
cd fop2
make install >>"$MYLOGFILE" 2>&1
mv ${AMP_WEB}/fop2 ${FOP_WEB}

#MANAGER
sed -i "s/\(manager_user= *\)\(.*\)/\1${AMPMGR_USER}/" /usr/local/fop2/fop2.cfg
sed -i "s/\(manager_secret= *\)\(.*\)/\1${ASTERISK_MGR_PW}/" /usr/local/fop2/fop2.cfg
sed -i "s,\(^\$DBPASS*\)\(.*\),\1 = '${ASTERISK_MGR_PW}';," ${FOP_WEB}/config.php

touch /etc/asterisk/extensions_override_freepbx.conf
cd /usr/local/fop2
sed -i "s,\(^\web_dir*\)\(.*\),\1 = /var/www/fop2," /usr/local/fop2/fop2.cfg
./generate_override_contexts.pl -write >>"$MYLOGFILE" 2>&1

# Aplicar permisos de carpeta y ficheros a fop2
chown -R asterisk:asterisk ${FOP_WEB}
chmod go-w ${FOP_WEB}/uploads

sed -i "s/^\s*\$DBNAME\s*=\s*'.*'\s*;\s*/\$DBNAME = 'fop2';/g" ${FOP_WEB}/config.php
sed -i "s/^\s*\$DBUSER\s*=\s*'.*'\s*;\s*/\$DBUSER = 'fop2';/g" ${FOP_WEB}/config.php
sed -i "s/^\s*\$DBPASS\s*=\s*'.*'\s*;\s*/\$DBPASS = '${FOP2_PW}';/g" ${FOP_WEB}/config.php
sed -i "s/^\s*\$language\s*=\s*".*"\s*;\s*/\$language = \"es\";/g" ${FOP_WEB}/config.php
sed -i "s/\(var language *= *\)\(.*\)/\1\"es\";/g" ${FOP_WEB}/js/presence.js

STOP=1
informa "$(gettext "Panel Operador Flash 2 instalado correctamente.")"

# FIX System: /dev/fd/62: No such file or directory
ln -s /proc/self/fd /dev/fd > /dev/null 2>&1

###-------------Instalando FREEPBX MODULE fop2admin------------------###

cd ${AMP_WEB}/admin/modules
wget_progress "$(gettext "Instalando módulo fop2admin")" http://download.fop2.com/fop2admin-${VER_FOP2_ADMIN}.tgz
tar zxf fop2admin-${VER_FOP2_ADMIN}.tgz >>"$MYLOGFILE" 2>&1
sed -i "s,<category>Third Party Addon</category>,<category>Settings-FOP2</category>," ${AMP_WEB}/admin/modules/fop2admin/module.xml
/var/lib/asterisk/bin/module_admin install fop2admin >>"$MYLOGFILE" 2>&1
rm fop2admin-${VER_FOP2_ADMIN}.tgz
amportal a reload >>"$MYLOGFILE" 2>&1

###-------------Instalando FREEPBX MODULE asternic_cdr---------------###

informa "$(gettext "Instalando módulo asternic_cdr")"
cp /usr/src/asternic_cdr-1.5.1.tgz ${AMP_WEB}/admin/modules/
tar zxf asternic_cdr-1.5.1.tgz >>"$MYLOGFILE" 2>&1
/var/lib/asterisk/bin/module_admin install asternic_cdr >>"$MYLOGFILE" 2>&1
rm asternic_cdr-1.5.1.tgz
amportal a reload >>"$MYLOGFILE" 2>&1

###-------------Instalando FREEPBX MODULE manualvozbox---------------###

informa "$(gettext "Instalando módulo manualvozbox")"
cp /usr/src/manualvozbox-${VER_MANUAL}.tar.gz ${AMP_WEB}/admin/modules/
tar zxf manualvozbox-${VER_MANUAL}.tar.gz >>"$MYLOGFILE" 2>&1
/var/lib/asterisk/bin/module_admin install manualvozbox >>"$MYLOGFILE" 2>&1
rm manualvozbox-${VER_MANUAL}.tar.gz
amportal a reload >>"$MYLOGFILE" 2>&1

###---------------Instalando Dynamic Route MODULE--------------------###

informa "$(gettext "Instalando módulo dynroute")"
cp /usr/src/dynroute-2.11.0.0.latest.tar.gz ${AMP_WEB}/admin/modules/
tar zxf dynroute-2.11.0.0.latest.tar.gz >>"$MYLOGFILE" 2>&1
sed -i "s,<category>Third Party Addon</category>,<category>Admin</category>," ${AMP_WEB}/admin/modules/dynroute/module.xml
/var/lib/asterisk/bin/module_admin install dynroute >>"$MYLOGFILE" 2>&1
rm dynroute-2.11.0.0.latest.tar.gz
amportal a reload >>"$MYLOGFILE" 2>&1

###---------------Instalando missedcallnotify MODULE-----------------###

informa "$(gettext "Instalando módulo missedcallnotify")"
cp /usr/src/missedcallnotify-0.5.tar.gz ${AMP_WEB}/admin/modules/
tar zxf missedcallnotify-0.5.tar.gz >>"$MYLOGFILE" 2>&1
#~ /var/lib/asterisk/bin/module_admin install missedcallnotify >>"$MYLOGFILE" 2>&1
rm missedcallnotify-0.5.tar.gz
amportal a reload >>"$MYLOGFILE" 2>&1

###---------------Instalando estado extensiones MODULE---------------###

informa "$(gettext "Instalando módulo extcfg")"
cp /usr/src/extcfg-2.11.0.0.tar.gz ${AMP_WEB}/admin/modules/
tar zxf extcfg-2.11.0.0.tar.gz >>"$MYLOGFILE" 2>&1
/var/lib/asterisk/bin/module_admin install extcfg >>"$MYLOGFILE" 2>&1
rm extcfg-2.11.0.0.tar.gz
amportal a reload >>"$MYLOGFILE" 2>&1

###---------------Instalando extensionsettings MODULE-----------------###

wget_progress "$(gettext "Instalando módulo extensionsettings")" http://mirror.freepbx.org/modules/release/2.11/extensionsettings-2.11.latest.tgz
tar zxf extensionsettings-2.11.latest.tgz >>"$MYLOGFILE" 2>&1
/var/lib/asterisk/bin/module_admin install extensionsettings >>"$MYLOGFILE" 2>&1
rm extensionsettings-2.11.latest.tgz
amportal a ma upgrade extensionsettings >>"$MYLOGFILE" 2>&1
amportal a reload >>"$MYLOGFILE" 2>&1

###---------------------Reinicio de servicios------------------------###

informa "$(gettext "Reiniciando Servicios...")"
/usr/local/sbin/amportal restart > /dev/null 2>&1
/etc/init.d/fop2 restart > /dev/null 2>&1

###-------------Instalando FREEPBX MODULE fw_langpacks---------------###

cd ${AMP_WEB}/admin/modules
informa "$(gettext "Instalando fw_langpacks")"
cp /usr/src/fw_langpacks.latest.tar.gz ${AMP_WEB}/admin/modules/
tar zxf ${AMP_WEB}/admin/modules/fw_langpacks.latest.tar.gz >>"$MYLOGFILE" 2>&1
rm fw_langpacks.latest.tar.gz
/var/lib/asterisk/bin/module_admin install fw_langpacks >>"$MYLOGFILE" 2>&1

###------------Configurando /etc/asterisk/ccss.conf------------------###

mv /etc/asterisk/ccss.conf /etc/asterisk/ccss.conf.back
ln -s /${AMP_WEB}/admin/modules/campon/etc/ccss.conf /etc/asterisk/ccss.conf

###--------------------Cambio visual de freepbx----------------------###
###-------------Instalando FREEPBX MODULE vozboxpbxskin--------------###

cd ${AMP_WEB}/admin/modules
informa "$(gettext "Instalando vozboxpbxskin")"
cp /usr/src/vozboxpbxskin-${VER_SKIN}.tar.gz ${AMP_WEB}/admin/modules/
tar zxf ${AMP_WEB}/admin/modules/vozboxpbxskin-${VER_SKIN}.tar.gz >>"$MYLOGFILE" 2>&1
rm vozboxpbxskin-${VER_SKIN}.tar.gz
/var/lib/asterisk/bin/module_admin install vozboxpbxskin >>"$MYLOGFILE" 2>&1
sed -i "s,'FreePBX','VozBox'," ${AMP_WEB}/admin/modules/dashboard/page.index.php

amportal a reload >>"$MYLOGFILE" 2>&1

###------Seleccionar el tono regional(/etc/dahdi/system.conf)--------###

lista "$(gettext "Seleccione Tone Region para configuración de Asterisk") (/etc/dahdi/system.conf):"
UTONE=$(cat /tmp/vozbox.dialog.$$)
if test "$UTONE" = ""
		then
		UTONE="es";
fi
echo "use asterisk; update freepbx_settings set value = '${UTONE}' where keyword = 'TONEZONE';" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO sipsettings VALUES ('sip_language','${UTONE}','','');" | mysql -u root -p${MYSQL_ROOT_PW}
echo "use asterisk; INSERT INTO iaxsettings VALUES ('iax_language','${UTONE}','','');" | mysql -u root -p${MYSQL_ROOT_PW}

/usr/src/freepbx/apply_conf.sh > /dev/null 2>&1
service apache2 restart > /dev/null 2>&1

###-----------------Instalar la tarjeta RDSI Sangoma-----------------###

if sangoma_detected
  then
	cd /usr/src
	echo "$(gettext "Detectada Tarjeta Sangoma")" >>"$MYLOGFILE" 2>&1
	informa "$(gettext "Determinando y preparando versión Sangoma ...")"
	wget_progress "$(gettext "Descargando Sangoma")"  $SANGOMA_TARBALLS -O /usr/src/wanpipe-current.tgz
	aptitude install -y -q $SANGOMA_BUILD_DEPENDS 2>/dev/null | progress_aptitude "$(eval_gettext "Instalando \$SANGOMA_BUILD_DEPENDS ...")"
	echo "$(gettext "CONFIGURANDO wanpipe...")"
	cd /usr/src
	asterisk -rx "core stop now" >&2
	tar zxf wanpipe-current.tgz	2>&1
	cd "/usr/src/`tar -tf "/usr/src/wanpipe-current.tgz" | head -1`"
	./Setup dahdi --with-zaptel="/usr/src/dahdi" \
                  --with-asterisk="/usr/src/asterisk" \
                  --no-zaptel-compile \
                  --silent \
                  >&2
    wanrouter hwprobe
    wancfg_dahdi
	sed -i "s,\(^\loadzone*\)\(.*\),\1 = $UTONE," /etc/dahdi/system.conf
	sed -i "s,\(^\defaultzone*\)\(.*\),\1 = $UTONE," /etc/dahdi/system.conf
    wanrouter start
    dahdi_cfg -vvv
	informa "$(gettext "wanpipe instalado.")"
  else
    progresoCORTO "$(gettext "GENERANDO CONFIGURACIÓN DAHDI...")"
	cd /etc/asterisk
	amportal stop >>"$MYLOGFILE" 2>&1
	/etc/init.d/dahdi start >>"$MYLOGFILE" 2>&1
	dahdi_scan >>"$MYLOGFILE" 2>&1
	dahdi_hardware >>"$MYLOGFILE" 2>&1
	dahdi_genconf modules >>"$MYLOGFILE" 2>&1
	dahdi_genconf >>"$MYLOGFILE" 2>&1
	sed -i "s,\(^\loadzone*\)\(.*\),\1 = $UTONE," /etc/dahdi/system.conf
	sed -i "s,\(^\defaultzone*\)\(.*\),\1 = $UTONE," /etc/dahdi/system.conf
	dahdi_cfg -vv >>"$MYLOGFILE" 2>&1
	/etc/init.d/dahdi restart > /dev/null 2>&1
	mv /etc/asterisk/chan_dahdi.conf /etc/asterisk/chan_dahdi.conf.back
	cat > /etc/asterisk/chan_dahdi.conf <<ENDLINE
; www.vozbox.es
; DAHDI telephony
;
; Configuration file
;
[channels]

context=default
switchtype=euroisdn
pridialplan=local
language=en
usecallerid=yes
hidecallerid=no
callwaiting=yes
usecallingpres=yes
callwaitingcallerid=yes
threewaycalling=yes
transfer=yes
canpark=yes
cancallforward=yes
callreturn=yes
echocancel=yes
immediate=no
echocancelwhenbridged=no
rxgain=0.0
txgain=0.0
group=0
callgroup=1
pickupgroup=1
resetinterval=never
;
faxdetect=both

#include dahdi-channels.conf
#include chan_dahdi_additional.conf
ENDLINE
fi

if ! digium_detected
  then
    echo "dahdi_dummy" >> /etc/dahdi/modules
fi
sed -i "s,\(language=*\)\(.*\),\1$UTONE," /etc/asterisk/chan_dahdi.conf
chown asterisk:asterisk /etc/asterisk/chan_dahdi.conf
amportal restart > /dev/null 2>&1
STOP=1

###-------Fijando timeout para grabación de llamadas en curso--------###

echo "featuredigittimeout = 1500" >> /etc/asterisk/features_general_custom.conf

###--cron para logrotate de asterisk (se ejecuta con el cron.daily)--###

touch /etc/logrotate.d/asterisk
cd /etc/logrotate.d/
echo "/var/log/asterisk/debug /var/log/asterisk/security_log /var/log/asterisk/freepbx.log /var/log/asterisk/freepbx_dbug /var/log/asterisk/freepbx_debug /var/log/asterisk/full /var/log/asterisk/messages /var/log/asterisk/cdr-csv/Master.csv /var/log/asterisk/queue_log {" >> asterisk
echo "   missingok" >> asterisk
echo "   daily" >> asterisk
echo "   rotate 7" >> asterisk
echo "   compress" >> asterisk
echo "   delaycompress" >> asterisk
echo "   notifempty" >> asterisk
echo "   sharedscripts" >> asterisk
echo "   create 0640 asterisk asterisk " >> asterisk
echo "   postrotate " >> asterisk
echo "   /bin/chown asterisk:asterisk /var/log/asterisk/* " >> asterisk
echo "   /usr/sbin/asterisk -rx 'logger reload' > /dev/null 2> /dev/null " >> asterisk
echo "   endscript " >> asterisk
echo "}" >> asterisk

sed -i "s,rotate 52,rotate 10," /etc/logrotate.d/apache2

###----------------cron remove fax de asterisk ----------------------###

touch /usr/local/bin/remove-faxes
cat > /usr/local/bin/remove-faxes << ENDLINE
#!/bin/bash
# remove-fax"
#
rm -fr /var/spool/asterisk/fax/*
rm -fr /tmp/*.pdf /tmp/*.tif
exit
ENDLINE
chmod +x /usr/local/bin/remove-faxes
echo "00 * * * 0 root /usr/local/bin/remove-faxes" >> /etc/crontab

###-----------------Instalando música adicional----------------------###

echo "$(gettext "INSTALANDO MÚSICA")" >>"$MYLOGFILE" 2>&1
cd /usr/src/
tar zxf mohmp3.tar.gz
rm -rf mohmp3.tar.gz
mv mohmp3/* /var/lib/asterisk/mohmp3/
chown -R asterisk:asterisk /var/lib/asterisk/mohmp3
informa "$(gettext "MÚSICA adicional instalada.")"

###--------------------------Log unique ID---------------------------###

echo "loguniqueid=yes" >> /etc/asterisk/cdr_mysql.conf

###----------Aplicando seguridad al Sistema contra ataques SIP-------###

mv /usr/src/sipcheck.pl /usr/local/bin/sipcheck.pl
chmod +x /usr/local/bin/sipcheck.pl
echo "00 *    * * *   root    /usr/local/bin/sipcheck.pl messages" >> /etc/crontab

###------------------Activando full log asterisk---------------------###

echo "full => notice,warning,error,debug,verbose" >> /etc/asterisk/logger_logfiles_custom.conf

###-----------------------Aplicando Permisos-------------------------###

informa "$(gettext "Aplicando Permisos a DAHDI...")"
chmod -R o-w /etc/amportal.conf
chown asterisk:asterisk /etc/asterisk/dahdi-channels.conf
chown asterisk:asterisk /etc/amportal.conf
chown asterisk:asterisk /etc/modprobe.d/dahdi.conf
chmod -R o-w /etc/{dahdi,asterisk}
chmod o-w /etc/modprobe.d/dahdi.conf
###----------------------Instalación Webmin--------------------------###

echo "$(gettext "INSTALANDO WEBMIN")" >>"$MYLOGFILE" 2>&1
cd /usr/src/
aptitude install -y -q apt-show-versions libnet-ssleay-perl libpam-runtime libauthen-pam-perl libapt-pkg-perl libio-pty-perl 2>/dev/null | progress_aptitude "$(gettext "INSTALANDO dependencias y paquetes webmin ...")"
descarga="webmin"
esperar_descarga "$descarga" || error "$(gettext "esperando descarga")"
progreso "$(eval_gettext "INSTALANDO webmin-\${VER_WEBMIN}...")"
tar zxf webmin-${VER_WEBMIN}-minimal.tar.gz
gunzip ./net.wbm.gz
gunzip ./time.wbm.gz
gunzip ./reinicio.wbm.gz
gunzip ./mount.wbm.gz
gunzip ./webmin.conf.tar.gz
gunzip ./hylafax.wbm.gz
rm -rf webmin-${VER_WEBMIN}-minimal.tar.gz

###-------------------preinstalación Webmin--------------------------###

mkdir /usr/src/webmin
mv /usr/src/webmin.conf.tar /usr/src/webmin/
cd /usr/src/webmin
tar -xf webmin.conf.tar
rm webmin.conf.tar
mkdir /etc/webmin
mv /usr/src/webmin/etc/* /etc/webmin/

###-----------------------instalación Webmin-------------------------###

cd /usr/src/webmin-${VER_WEBMIN}
sed -i 's/read config_dir/\echo "Instalación automatizada..."/g' "/usr/src/webmin-${VER_WEBMIN}/setup.sh"
./setup.sh /usr/bin/webmin >>"$MYLOGFILE" 2>&1
cd /usr/bin/webmin/

###-----------------postinstalación Webmin---------------------------###

echo "$(gettext "Configurando Webmin")" >>"$MYLOGFILE" 2>&1
STOP=1
informa "$(eval_gettext "webmin-\${VER_WEBMIN} instalado.")"
progresoCORTO "$(eval_gettext "Configurando webmin-\${VER_WEBMIN}")"

###--------instala módulos y actualiza enlaces simbólicos------------###

./install-module.pl /usr/src/net.wbm >>"$MYLOGFILE" 2>&1
./install-module.pl /usr/src/time.wbm >>"$MYLOGFILE" 2>&1
./install-module.pl /usr/src/mount.wbm >>"$MYLOGFILE" 2>&1
./install-module.pl /usr/src/reinicio.wbm >>"$MYLOGFILE" 2>&1

mv /usr/src/webmin/init.d/webmin /etc/init.d/webmin
update-rc.d webmin defaults >>"$MYLOGFILE" 2>&1

###--------------modifico index del módulo init----------------------###

cp /usr/bin/webmin/init/index.cgi /usr/bin/webmin/init/index-orig.cgi

###-----------------configuración módulo init------------------------###

mv -f /usr/src/webmin/init/* /usr/bin/webmin/init/
mv -f /usr/src/webmin/etc-init/* /etc/webmin/init/
mv -f /usr/src/webmin/bin/* /usr/bin/webmin/init/

###------------------configuración módulo net------------------------###

mv /usr/src/webmin/net/* /etc/webmin/net/

###--------------cambio mensaje en módulo net------------------------###

sed -i "s/cortar el acceso a Webmin.$/cortar el acceso a Webmin.<br><b>ATENCION:<\/b> Al modificar la IP del Servidor VozBox, recuerde volver a generar el fichero aastra.cfg desde Comandos Personalizados./" /usr/bin/webmin/net/lang/es

###------------------configuración módulo time-----------------------###

mv /usr/src/webmin/time/* /etc/webmin/time/
STOP=1
informa "$(eval_gettext "webmin-\${VER_WEBMIN} instalado.")"

###--------------configuración password usuario webmin---------------###

selecciona "$(gettext "Introduce el password para el usuario administrador (admin) de webmin:")" $WEBMIN_PW
WEBMIN_PW=$(cat /tmp/vozbox.dialog.$$)
informa "$(eval_gettext "Password: \$WEBMIN_PW")"
sleep 0.5
/usr/bin/webmin/changepass.pl /etc/webmin admin $WEBMIN_PW >>"$MYLOGFILE" 2>&1
echo "" >> /root/passwords
echo "# WEBMIN_PW" >> /root/passwords
echo "WEBMIN_PW=\"${WEBMIN_PW}\";" >> /root/passwords
echo "################ $(gettext "FIN PASSWORDS") #####################" >> /root/passwords

###-------conexiones exclusivamente desde la red corporativa---------###
echo "libwrap=" >> /etc/webmin/miniserv.conf
echo "alwaysresolve=0" >> /etc/webmin/miniserv.conf
echo "allow=127.0.0.1 LOCAL" >> /etc/webmin/miniserv.conf

/etc/init.d/webmin restart > /dev/null 2>&1

###------------Configuración de Reloj interno para Red---------------###

echo "statsdir /var/log/ntpstats/" >> /etc/ntp.conf
echo "server 127.127.1.0" >> /etc/ntp.conf
echo "fudge 127.127.1.0 stratum 10" >> /etc/ntp.conf
echo "server es.pool.ntp.org maxpool 12" >> /etc/ntp.conf
echo "server europe.pool.ntp.org maxpool 12" >> /etc/ntp.conf
echo "disable auth" >> /etc/ntp.conf
echo "broadcastclient" >> /etc/ntp.conf

###------------crear fichero /var/www/html/webmin.php----------------###

mv /usr/src/webmin.php ${AMP_WEB}/webmin.php

###-----------------Incrementar el file descriptor-------------------###

echo -e "root\t-\tnofile\t100000" >> /etc/security/limits.conf
echo -e "asterisk\t-\tnofile\t100000" >> /etc/security/limits.conf

###-----------------------Reiniciando Apache-------------------------###

informa "$(gettext "Iniciando apache2 webserver")"
apache2ctl restart > /dev/null 2>&1
start_mysql_server > /dev/null 2>&1
sleep 1

###----------Creando script para inicio de freepbx y fop2------------###
echo "$(gettext "Creando script para inicio de freepbx y fop2")" >>"$MYLOGFILE" 2>&1
INIT_SCRIPT="/etc/init.d/freepbx"
  cat > "$INIT_SCRIPT" <<EOF
#!/bin/bash

AMPORTAL_BIN=/usr/local/sbin/amportal
### BEGIN INIT INFO
# Provides:             freepbx
# Required-Start:    \$network \$syslog \$named \$local_fs \$remote_fs $all
# Required-Stop:     \$network \$syslog \$named \$local_fs \$remote_fs $all
# Should-Start:      dahdi misdn lcr wanrouter mysql
# Should-Stop:       dahdi misdn lcr wanrouter mysql
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    FreePBX-VozBox
# Description:          mas que una centralita
### END INIT INFO
# <===================================

if [ ! -x \${AMPORTAL_BIN} ]; then
        echo "error : amportal binary can not be found (${AMPORTAL_BIN})"
        exit 0
fi

start() {
        echo "Iniciando FreePBX ..."
        \${AMPORTAL_BIN} start
}

stop() {
        echo "Parando FreePBX ..."
        \${AMPORTAL_BIN} stop
}

case "\$1" in
  start)
        start
        ;;

  stop)
        stop
        ;;

  restart)
        stop
        start
        ;;

  *)
        echo "Utilice: \$0 {start|stop|restart}"
        exit 1
esac

exit 0
EOF

# ... marcar como ejecutable y permitir los niveles de ejecución
chmod 755 "$INIT_SCRIPT"
update-rc.d freepbx defaults
/usr/local/bin/permisonidos &>/dev/null 2>&1
sleep 0.5
informa " ------ $(gettext "Hasta aquí instalación FreePBX") ------"
sleep 0.5

###---------modificación de datos de email de correo vocal-----------###

mv /etc/asterisk/voicemail.conf /etc/asterisk/voicemail.conf-orig
mv /usr/src/voicemail.conf /etc/asterisk/
chown asterisk:asterisk /etc/asterisk/voicemail.conf
chown -R asterisk:asterisk /var/spool/asterisk/
chmod -R o-w /var/spool/asterisk/voicemail

###---------instación de módulo web-fax-ari para FAX Digium----------###
cd ${AMP_WEB}/recordings/modules/
cp /usr/src/webfax-ari-module.tar.gz ${AMP_WEB}/recordings/modules/
tar zxf webfax-ari-module.tar.gz >>"$MYLOGFILE" 2>&1
mv sendfaxnotify.php /var/lib/asterisk/bin/sendfaxnotify.php >>"$MYLOGFILE" 2>&1
dos2unix /var/lib/asterisk/bin/sendfaxnotify.php >>"$MYLOGFILE" 2>&1

cat > /etc/asterisk/extensions_custom.conf <<"EOF"
[outboundfax]
exten => s,1,Set(FAXOPT(filename)=${FAXFILE})
exten => s,n,Set(FAXOPT(ecm)=yes)
exten => s,n,Set(FAXOPT(headerinfo)=${FAXHEADER})
exten => s,n,Set(FAXOPT(localstationid)=${LOCALID})
exten => s,n,Set(FAXOPT(maxrate)=14400)
exten => s,n,Set(FAXOPT(minrate)=2400)
exten => s,n,SendFAX(${FAXFILE},d)
exten => s,n,System(${ASTVARLIBDIR}/bin/sendfaxnotify.php INIT "${EMAIL}" "${DESTINATION}" "${TIMESTAMP}" "NO_STATUS" "NO_PAGES")
exten => h,1,NoOp(FaxStatus : ${FAXSTATUS})
exten => h,n,NoOp(FaxStatusString : ${FAXSTATUSSTRING})
exten => h,n,NoOp(FaxError : ${FAXERROR})
exten => h,n,NoOp(RemoteStationID : ${REMOTESTATIONID})
exten => h,n,NoOp(FaxPages : ${FAXPAGES})
exten => h,n,NoOp(FaxBitRate : ${FAXBITRATE})
exten => h,n,NoOp(FaxResolution : ${FAXRESOLUTION})
exten => h,n,System(${ASTVARLIBDIR}/bin/sendfaxnotify.php NOTIFY "${EMAIL}" "${DESTINATION}" "${TIMESTAMP}" "${FAXSTATUSSTRING}" "${FAXPAGES}")
; end of outboundfax context
EOF

rm webfax-ari-module.tar.gz
chown asterisk:asterisk /var/www/html/recordings/modules/sendfax.module
chown asterisk:asterisk /var/lib/asterisk/bin/sendfaxnotify.php
amportal a reload >>"$MYLOGFILE" 2>&1

###----Cambio de puerto rtp de inicio en /etc/asterisk/rtp.conf------###

sed -i "s/\(rtpstart= *\)\(.*\)/\110001/" /etc/asterisk/rtp.conf

# FIX chown: cannot access `/dev/tty9': No such file or directory
mknod /dev/tty9 c 4 9
#TAG:FIX FOR OPENVZ COMPATIBILITY
sed -i 's/chown $AMPMGR_USER \/dev\/tty9/#chown $AMPMGR_USER \/dev\/tty9/g' /var/lib/asterisk/bin/freepbx_engine

#################### Ajuste de Zonas horarias ##########################

echo "$(gettext "Ajustando Zona Horaria")" >>"$MYLOGFILE" 2>&1
informa "$(gettext "Ajustando Zona Horaria")..."

TZONE=`cat /etc/timezone`;
ln -sf /usr/share/zoneinfo/${TZONE} /etc/localtime
sed -i "s/;date.timezone/date.timezone/" /etc/php5/apache2/php.ini
sed -i "s,\(date.timezone = *\)\(.*\),\1 \"${TZONE}\"," /etc/php5/apache2/php.ini
sed -i "s/;date.timezone/date.timezone/" /etc/php5/cli/php.ini
sed -i "s,\(date.timezone = *\)\(.*\),\1 \"${TZONE}\"," /etc/php5/cli/php.ini

sed -i "s/iso-8859-1/utf8/" /etc/php5/apache2/php.ini
sed -i "s/;default_charset/default_charset/" /etc/php5/apache2/php.ini

informa "$(gettext "Instalando autentificación SMTP...")"
aptitude install -y -q sasl2-bin libsasl2-2 libsasl2-modules 2>/dev/null | progress_aptitude "$(gettext "Instalando autentificación SMTP...")"
informa "$(gettext "Configurando auth smtp...")"
informa "$(gettext "Configurando inicio automático de autenticación por sas")"
sed -i "s/\(^START=no\)/START=yes/" /etc/default/saslauthd

###------------AGREGANDO credenciales para smtp relay----------------###

cd /etc/postfix/sasl
selecciona "$(gettext "Introduce email origen desde donde enviará tu Servidor SMTP los correos ej(usuario@dominio.es):")" $smtpmail
usmtpmail=$(cat /tmp/vozbox.dialog.$$)
informa "$(eval_gettext "mail: \$usmtpmail")"
sleep 0.5
selecciona "$(gettext "Introduce usuario para autenticarte en el Servidor SMTP ej(usuario.dominio.es):")" $smtpusua
usmtpusua=$(cat /tmp/vozbox.dialog.$$)
informa "$(eval_gettext "Usuario: \$usmtpusua")"
sleep 0.5
selecciona "$(gettext "Introduce password para autenticarte en el Servidor SMTP:")" $smtppass
usmtppass=$(cat /tmp/vozbox.dialog.$$)
informa "$(eval_gettext "Password: \$usmtppass")"
sleep 0.5
selecciona "$(gettext "Introduce email destino donde llegarán las notificaciones de tu Servidor ej(usuario@dominio.es):")" $destinomail
destinomail=$(cat /tmp/vozbox.dialog.$$)
informa "$(eval_gettext "mail: \$destinomail")"
sleep 0.5
rlh=`sed -ne '/^relayhost =/p' /etc/postfix/main.cf | cut -d" " -f3`
echo -ne "${rlh}\t${usmtpusua}:${usmtppass}\n" > /etc/postfix/sasl/sasl_passwd
postmap sasl_passwd

###--------------borrado de fichero plano con passwords--------------###

rm sasl_passwd

###------------------generando smtp masquerade-----------------------###

cd /etc/postfix
echo -ne "asterisk@`cat /etc/mailname`\t${usmtpmail}\n" > /etc/postfix/generic
postmap generic

###----------------agregando autentificación al main.cf--------------###

echo "## sasl" >> /etc/postfix/main.cf
echo "smtp_sasl_auth_enable = yes" >> /etc/postfix/main.cf
echo "smtp_sasl_type = cyrus" >> /etc/postfix/main.cf
echo "smtp_sasl_password_maps = hash:/etc/postfix/sasl/sasl_passwd" >> /etc/postfix/main.cf
echo "smtp_sasl_security_options = noanonymous" >> /etc/postfix/main.cf
echo "smtp_generic_maps = hash:/etc/postfix/generic" >> /etc/postfix/main.cf

informa "$(gettext "Zona Horaria ajustada.")"

###-----------cambio dirección correo del correo vocal---------------###

cp /etc/asterisk/voicemail.conf /etc/asterisk/voicemail.conf-orig2
sed -i "s/\(^serveremail= *\)\(.*\)/serveremail=${usmtpmail}/" /etc/asterisk/voicemail.conf

###------------cambio dirección correo de sipcheck.pl----------------###

sed -i "s,origen@vozbox.es,${usmtpmail}," /usr/local/bin/sipcheck.pl
sed -i "s,destino@vozbox.es,${destinomail}," /usr/local/bin/sipcheck.pl
sed -i "s,PBX-VozBox,PBX-VozBox-$HOST_NAME," /usr/local/bin/sipcheck.pl

###-------------cambio directorio por defecto de tftpd---------------###

cp /etc/inetd.conf /etc/inetd.conf-orig
sed -i 's/\/srv\/tftp/\/tftpboot/' /etc/inetd.conf

###-------conexiones exclusivamente desde la red corporativa---------###
echo "sshd : all : allow" >> /etc/hosts.allow
echo "in.tftpd : 127.0.0.1 ${SUBNET}/${NETWORK_MASK} : allow" >> /etc/hosts.allow
echo "all : all : deny" >> /etc/hosts.allow

########################################################################
########################################################################

echo "$(gettext "CONFIGURACIÓN DNSMASQ")" >>"$MYLOGFILE" 2>&1
progresoCORTO "$(gettext "CONFIGURANDO DNSMASQ")"
apt-get -y install dnsmasq* >>"$MYLOGFILE" 2>&1
less /etc/resolv.conf|sed 's/nameserver /server=/g'|grep server >/etc/dnsmasq.d/dns.conf
echo "server=8.8.8.8" >> /etc/dnsmasq.d/dns.conf
echo "listen-address=127.0.0.1" >> /etc/dnsmasq.d/dns.conf
echo "listen-address=::1" >> /etc/dnsmasq.d/dns.conf
echo "bind-interfaces" >> /etc/dnsmasq.d/dns.conf
sed -i '/nameserver/d' /etc/resolv.conf
echo "nameserver 127.0.0.1" > /etc/resolv.conf
echo "resolv-file=/etc/resolv.conf" >> /etc/dnsmasq.conf
service dnsmasq restart > /dev/null 2>&1
# Cambiando allow-hotplug eth0 por auto auto eth0 en /etc/network/interfaces
sed -i "s,allow-hotplug,auto," /etc/network/interfaces
service networking restart > /dev/null 2>&1
sleep 0.5
STOP=1
informa "$(gettext "DNSMASQ configurado.")"

########################################################################
###-----------Evitar entrada automática de usuario root--------------###
sed -i "s,1:2345:once:/bin/login -f root tty1 </dev/tty1 >/dev/tty1 2>&1,1:2345:respawn:/sbin/getty 38400 tty1," /etc/inittab

########################################################################

echo "$(gettext "RECONFIGURANDO PHP-MAIL-MIME PHP-MAIL-MIMECODE")" >>"$MYLOGFILE" 2>&1
progresoCORTO "$(gettext "RECONFIGURANDO PHP-MAIL-MIME PHP-MAIL-MIMECODE")"
aptitude install php-mail-mime php-mail-mimedecode >>"$MYLOGFILE" 2>&1
STOP=1

###----------Aplicando reglas de seguridad con IPTABLES--------------###

echo "$(gettext "Aplicando reglas de seguridad con IPTABLES")" >>"$MYLOGFILE" 2>&1
apt-get -y install iptables-persistent

###---------------Instalando Config Security Firewall----------------###

whiptail --title "$TITLE" --msgbox "$(gettext "Instalación y configuración de ConfigSecurityFirewall.")" 14 80 0
cd /usr/src/
wget_progress "$(gettext "Instalando paquetes y dependencias ... Por favor espere ...")" http://www.configserver.com/free/csf.tgz
tar zxf csf.tgz
cd csf/
./install.sh >/dev/null 2>&1
cp /usr/src/csf.pignore /etc/csf/csf.pignore
sed -i 's/IPV6 = "1"/IPV6 = "0"/g' /etc/csf/csf.conf
sed -i 's/TESTING = "1"/TESTING = "0"/g' /etc/csf/csf.conf
sed -i 's/RESTRICT_SYSLOG = "0"/RESTRICT_SYSLOG = "3"/g' /etc/csf/csf.conf
sed -i "s,\(TCP_IN = *\)\(.*\),\1\"587\"," /etc/csf/csf.conf
sed -i "s/\(TCP_OUT = *\)\(.*\)/\1\"80,587\"/" /etc/csf/csf.conf
sed -i "s/\(UDP_IN = *\)\(.*\)/\1\"67,68\"/" /etc/csf/csf.conf
sed -i "s/\(UDP_OUT = *\)\(.*\)/\1\"67,68,123\"/" /etc/csf/csf.conf
csf -a ${SUBNET}/24 >/dev/null 2>&1
csf -u >/dev/null 2>&1
csf -r >/dev/null 2>&1
/usr/bin/webmin/install-module.pl /etc/csf/csfwebmin.tgz /etc/webmin >/dev/null 2>&1
whiptail --title "$TITLE" --msgbox "$(gettext "ConfigSecurityFirewall ha sido instalado correctamente. Vaya al menú webmin 'Servidores' para configurar y habilitar el Firewall.")" 14 80 0

###-------------------Instalación phpsysinfo-------------------------###

cd /var/www
wget_progress "$(gettext "Instalación de PHPSysInfo.")" http://sourceforge.net/projects/phpsysinfo/files/phpsysinfo/${VER_PHPSYSINFO}/phpsysinfo-${VER_PHPSYSINFO}.tar.gz
tar xzf phpsysinfo-${VER_PHPSYSINFO}.tar.gz
ln -s /var/www/phpsysinfo-${VER_PHPSYSINFO} /var/www/phpsysinfo
mv /var/www/phpsysinfo/phpsysinfo.ini.new /var/www/phpsysinfo/phpsysinfo.ini

sed -i "s,\(DEFAULT_TEMPLATE=*\)\(.*\),\1\"cream\"," /var/www/phpsysinfo/phpsysinfo.ini
sed -i "s,\(DEFAULT_LANG=*\)\(.*\),\1\"es\"," /var/www/phpsysinfo/phpsysinfo.ini
sed -i "s,\(SHOW_PICKLIST_TEMPLATE=*\)\(.*\),\1false," /var/www/phpsysinfo/phpsysinfo.ini
sed -i "s,\(LOAD_BAR=*\)\(.*\),\1true," /var/www/phpsysinfo/phpsysinfo.ini

# fix template
mv /var/www/phpsysinfo/templates/phpsysinfo.css /var/www/phpsysinfo/templates/phpsysinfo.css.old
cp /var/www/phpsysinfo/templates/cream.css /var/www/phpsysinfo/templates/phpsysinfo.css

cat > /etc/apache2/conf.d/phpsysinfo.conf <<ENDLINE
Alias /phpsysinfo "/var/www/phpsysinfo/"
<Directory "/var/www/phpsysinfo/">
    Options FollowSymLinks
    Order allow,deny
    allow from 127.0.0.1 ${SUBNET}/${NETWORK_MASK}
</Directory>
ENDLINE
rm /var/www/phpsysinfo-${VER_PHPSYSINFO}.tar.gz

cat > /etc/apache2/sites-available/default <<ENDLINE
<VirtualHost *:80>
        ServerAdmin webmaster@localhost

        DocumentRoot ${AMP_WEB}
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory ${AMP_WEB}/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride None
                Order deny,allow
                deny from all
                allow from 127.0.0.1 ${SUBNET}/${NETWORK_MASK}
        </Directory>

        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order deny,allow
                deny from all
                allow from 127.0.0.1 ${SUBNET}/${NETWORK_MASK}
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
ENDLINE

###-------------------preinstalación AvantFAX------------------------###

mkdir /var/www/avantfax
cat > /var/www/avantfax/index.php <<ENDLINE
<HTML LANG="es">
<HEAD>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<div  style="position:absolute; overflow:hidden; left:100px; top:250px; "><img src="/admin/images/VozBoxA2_peq.png" alt="" border=0></div>
<div id="container">
<div style="position:absolute; overflow:hidden; top:150; " align="left">
<table>
<tr>
<td>
<font class="ws12" >
Este servicio no lo tiene contratado. Si desea contratarlo p&oacute;ngase en contacto con VozBox.
</font>
</td>
</tr>
</table>
</div>
</div>
</body>
</html>
ENDLINE
chown -R asterisk. /var/www/avantfax

cat > /etc/apache2/conf.d/avantfax.conf <<ENDLINE
Alias /avantfax "/var/www/avantfax/"
<Directory "/var/www/avantfax/">
    Options FollowSymLinks
    Order deny,allow
    deny from all
    allow from 127.0.0.1 ${SUBNET}/${NETWORK_MASK}
</Directory>
ENDLINE

###--------------------preinstalación aastra-------------------------###
chown -R asterisk. /var/www/aastra
cat > /etc/apache2/conf.d/aastra.conf <<ENDLINE
Alias /aastra "/var/www/aastra/"
<Directory "/var/www/aastra/">
    Options FollowSymLinks
    Order deny,allow
    deny from all
    allow from 127.0.0.1 ${SUBNET}/${NETWORK_MASK}
</Directory>
ENDLINE

###-----------------------instalación fop2---------------------------###
chown -R asterisk. ${FOP_WEB}
cat > /etc/apache2/conf.d/fop2.conf <<ENDLINE
Alias /fop2 "/var/www/fop2/"
<Directory "/var/www/fop2/">
    Options FollowSymLinks
    Order deny,allow
    deny from all
    allow from 127.0.0.1 ${SUBNET}/${NETWORK_MASK}
</Directory>
ENDLINE

apache2ctl restart > /dev/null 2>&1

echo "#include extensions_aastra.conf" >> /etc/asterisk/extensions_override_freepbx.conf
########################################################################
}
########################################################################
########################## FIN INSTALACIÓN #############################
########################################################################

report ()
{
#whiptail --title "$TITLE" --msgbox "$(gettext "Para mejorar o solucionar algún problema en la instalación póngase en contacto a través de los siguientes mails:\n\ndavervozbox@gmail.com\nsoporte@vozbox.es\n\nEstaremos encantados de poder ayudar y mejorar la instalación para beneficio de todos.\n\nGracias.")" 14 80 0
#whiptail --title "$TITLE" --msgbox "$(gettext "Durante la instalación se ha generado un archivo con todos los passwords utilizados en la ruta '/root/passwords'\n\nLos comandos accesibles desde 'vozbox-help' hacen uso de estos passwords para su instalación y compilación por lo que es de vital importancia, no eliminar este archivo.\nGracias.")" 14 80 0
#whiptail --title "$TITLE" --msgbox "$(eval_gettext "GRACIAS por utilizar VozBox.\n\nAcceda a su servidor VozBox en http:// \$IP_ADDRESS")" 14 80 0

###---------------Borrado de ficheros de instalación-----------------###

informa "$(gettext "Limpiando SCRIPTS instalación...")"
gzip -f ${MYLOGFILE}
sleep 0.5
apt-get remove rpcbind -y &>/dev/null 2>&1
apt-get remove nfs-common -y &>/dev/null 2>&1
apt-get autoremove -y &>/dev/null 2>&1
apt-get autoclean &>/dev/null 2>&1
apt-get clean &>/dev/null 2>&1
history -c
rm /usr/local/sbin/vozbox_clean.sh
rm /tmp/vozbox.dialog.*
rm /usr/src/sourceforge.tar.gz
echo "$(gettext "Instalación de Asterisk realizada con éxito.")" >>"$MYLOGFILE" 2>&1
}

main 2>>"$MYLOGFILE"
report
if (pregunta "$(gettext "El servidor debe reiniciarse para completar la instalación.\n\nReiniciar el Servidor ahora?")")
then
	shutdown -r now;
else
	clear;
	vozbox-help;
fi
exit 0
